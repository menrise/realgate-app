
import { observable, action, autorun, computed, reaction, toJS } from 'mobx'
import { Alert } from 'react-native'
import { meetsTime, age, timeFormatDB, maxBirthDate, timeFormatUser, isBeforeThanNow, isAfterThanNow, periodBetween, nowMoment } from './../../helpers/time'
import { defaults, get, cloneDeep } from 'lodash'
import Api from './../../apis/Api'
import Facebook from './../../apis/Facebook'
import Instagram from './../../apis/Instagram'
import Vkontakte from './../../apis/Vkontakte'
import Google from './../../apis/Google'
import { Location, Permissions } from 'expo'
import { parse_jwt, clearMeetPlacesAdditionDoubles } from './../../helpers/misc'
import { composite_meet_id } from './../../helpers/names'
import { showMessage } from 'react-native-flash-message'
import remotedev from '@hlhr202/mobx-remotedev'// /lib/dev
import { cloneViaJson } from './../../helpers/misc'
import ws from './../../apis/Ws'
import { ignore } from 'mobx-sync'
import { regularMeetsUpdateFromServer } from './../../helpers/requests'
import { playSound } from './../../helpers/sounds'


@remotedev({ name: 'MainState', remote: false, global: true}) //, hostname: '192.168.1.3', port: 8000
class MainState {

  @ignore api = new Api()
  @ignore facebook = new Facebook()
  @ignore vkontakte = new Vkontakte()
  @ignore google = new Google()
  @ignore instagram = new Instagram()
  @ignore navigation
  @ignore ws = ws
  @ignore @observable isMapDataLoading = false


  @observable user_id
  @observable jwtToken = ''
  @observable sex = 0
  @observable sex_need = 0
  @observable birth_date = ''
  @observable user_name = ''
  @observable phone_number = '+7'
  @observable min_birth_date = '1950-01-01'
  @observable max_birth_date = maxBirthDate()
  @observable user_group = 0
  @observable meets = []
  @observable sexes = [1, 2] //1 - man, 2 - woman
  @observable meet_places = [] //google api place
  @observable meet_places_addition = [] //added places via near meet push (added to meet_places when initPlaces)
  @observable current_meet_place = {}
  @observable user_location = {coords: { latitude: 55.75775, longitude: 37.61708}} //default coordinate - moskow
  @observable currentMeetPlaceModalVisible = false
  @ignore @observable orderMeetInProcess = false
  @ignore @observable unorderMeetInProcess = false
  @observable saveSettingsInProcess = false  
  @observable user_meets = []
  @observable meetsLoading = false
  @observable selectedMyMeet = false
  @observable userMeetsInPorgressInWaitPeriod = []
  @observable userMeetsInPorgress = [] //include meets - userMeetsInPorgressInWaitPeriod
  @observable userMeetsPast = []
  @observable userMeetsFuture = []
  @observable meetsRealStartTime = []
  @observable lastPushNotification = {}
  @observable expo_push_token_registered = false
  @observable server_needs_device_time = false
  @observable meetPlacesByCoordsCache = []
  @observable googleImagesCache = []
  @observable meetPlacesCache = []
  @observable user_latitude_at_server = 0
  @observable user_longitude_at_server = 0
  @observable push_initiated_popup = {}
  @observable push_initiated_popup_google_place_id_for_query = null


  meetDuration = 60 //mins
  meetWaitPeriod = 15 //mins
  meetGroupsTemplate = { 
    meetGroup1: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 3, maxWoman: 3, sex_composition_id: 1, title: '3 на 3, М+Ж, младше 19'},
    meetGroup2: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 3, maxWoman: 3, sex_composition_id: 1, title: '3 на 3, М+Ж, 19 - 35 лет'},
    meetGroup3: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 3, maxWoman: 3, sex_composition_id: 1, title: '3 на 3, М+Ж, 35+ лет'},
    meetGroup4: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 4, maxWoman: 0, sex_composition_id: 2, title: 'Только М, младше 19'},
    meetGroup5: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 4, maxWoman: 0, sex_composition_id: 2, title: 'Только М, 19 - 35 лет'},
    meetGroup6: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 4, maxWoman: 0, sex_composition_id: 2, title: 'Только М, 35+ лет'},
    meetGroup7: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 0, maxWoman: 4, sex_composition_id: 3, title: 'Только Ж, младше 19'},
    meetGroup8: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 0, maxWoman: 4, sex_composition_id: 3, title: 'Только Ж, 19 - 35 лет'},
    meetGroup9: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 0, maxWoman: 4, sex_composition_id: 3, title: 'Только Ж, 35+ лет'},

    //1vs1
    meetGroup10: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 1, maxWoman: 1, sex_composition_id: 1, title: '1 на 1, М+Ж, младше 19'},
    meetGroup11: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 1, maxWoman: 1, sex_composition_id: 1, title: '1 на 1, М+Ж, 19 - 35 лет'},
    meetGroup12: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 1, maxWoman: 1, sex_composition_id: 1, title: '1 на 1, М+Ж, 35+ лет'},
    meetGroup13: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 2, maxWoman: 0, sex_composition_id: 2, title: '1 на 1, Только М, младше 19'},
    meetGroup14: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 2, maxWoman: 0, sex_composition_id: 2, title: '1 на 1, Только М, 19 - 35 лет'},
    meetGroup15: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 2, maxWoman: 0, sex_composition_id: 2, title: '1 на 1, Только М, 35+ лет'},
    meetGroup16: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 0, maxWoman: 2, sex_composition_id: 3, title: '1 на 1, Только Ж, младше 19'},
    meetGroup17: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 0, maxWoman: 2, sex_composition_id: 3, title: '1 на 1, Только Ж, 19 - 35 лет'},
    meetGroup18: {value: '', timeObject: null, timeFormatUser: '', timeFormatDB: '', memberOfMeet: false, manСount: 0, womanСount: 0, maxMan: 0, maxWoman: 2, sex_composition_id: 3, title: '1 на 1, Только Ж, 35+ лет'},
    

  } //sex_composition_id is - 1 - poly sex (man and woman), 2 mono sex (man), 3 mono sex (woman)
  

  constructor() {
    //this.clear()
    this.updateMeetGroupsTemplateTime()

    autorun( () => this.getUserId() )
    autorun( () => this.addPlacesToUserMeets() )

    //add meets to places when gets meets update
    reaction(
      () => this.meets,
      meets => this.addMeetsDataToPlaces(meets)
    )

    //meet separator (past, future, inProgress, etc)
    const meetTimeSeparator = setInterval( () => {
      this.setUserMeetsTimeArrays()
    }, 1000)

    //regular meets update gotten from server
    const regularMeetsUpdate = setInterval( () => {
      regularMeetsUpdateFromServer()
    }, 1000 * 60 * 5) //every 5 min
    
    //console.log('MainState: ', this)
  }

  @action.bound
  set(...args){
    args.map( (arg) => {
      this[arg.name] = arg.value
    })
  }

  @action.bound
  addMeetPlaceAddition(meet_place){
    const meet_place_cleared = clearMeetPlacesAdditionDoubles(this.meet_places_addition, [meet_place])
    if(meet_place_cleared.length){
      this.meet_places_addition.push(meet_place)
    }
  }

  getMeetGroupData(meetGroup){
    return this.meetGroupsTemplate['meetGroup' + meetGroup]
  }

  @action.bound
  addMeetPlacesByCoordsCache(meetPlacesByCoords){
    this.meetPlacesByCoordsCache.push(meetPlacesByCoords)
  }

  @action.bound
  addMeetPlacesCache(meetPlace){
    this.meetPlacesCache.push(meetPlace)
  }  

  @action.bound
  addGoogleImagesCache(googleImage){
    this.googleImagesCache.push(googleImage)
  }

  getMeetsRealStartTime(meetId){
    return this.meetsRealStartTime.find( (meet) => meet.id === meetId )
  }

  @action.bound
  setMeetsRealStartTime(meetId, realStartTime){
    this.clearOldMeetsRealStartTime()
    let meet = this.getMeetsRealStartTime(meetId)

    if(!meet){//not rewrite
      this.meetsRealStartTime.push( {id: meetId, realStartTime: realStartTime} )
    }

    return this.getMeetsRealStartTime(meetId)
  }

  @action.bound
  clearOldMeetsRealStartTime(){
    if(this.meetsRealStartTime.length === 0) return

    const meetsRealStartTimeFresh = this.meetsRealStartTime.filter( (meet) => {
      const secondsAfterRealStart = periodBetween(nowMoment(), meet.realStartTime, null, true)

      if(secondsAfterRealStart < 60 * 60){ // after 1 hour is rotten
        return true
      } 
    })

    this.meetsRealStartTime = meetsRealStartTimeFresh 
  }


  async approveMembering(meet_id){
    const res = await this.api.approveMembering(meet_id) 
    await this.getMeets(true)
  }


  async unapproveMembering(meet_id){
    const res = await this.api.unapproveMembering(meet_id) 
    await this.getMeets(true)
  }


  @action.bound
  setUserMeetsTimeArrays(){

    const userMeetsInPorgressInWaitPeriod = this.getUserMeetsInPorgressInWaitPeriod()
    if( JSON.stringify(this.userMeetsInPorgressInWaitPeriod) !== JSON.stringify(userMeetsInPorgressInWaitPeriod) ){
      this.userMeetsInPorgressInWaitPeriod = userMeetsInPorgressInWaitPeriod
    }

    const userMeetsInPorgress = this.getUserMeetsInPorgress()
    if( JSON.stringify(this.userMeetsInPorgress) !== JSON.stringify(userMeetsInPorgress) ){
      if(!this.userMeetsInPorgress.length && userMeetsInPorgress.length){
        playSound('roundChange') //meet start signal
      }
      this.userMeetsInPorgress = userMeetsInPorgress
    }

    const userMeetsPast = this.getUserMeetsPast()
    if( JSON.stringify(this.userMeetsPast) !== JSON.stringify(userMeetsPast) ){
      this.userMeetsPast = userMeetsPast

      if(this.userMeetsPast.length !== userMeetsPast.length){
        this.getMeets(true)//download likes after like time
      }
    }

    const userMeetsFuture = this.getUserMeetsFuture()
    if( JSON.stringify(this.userMeetsFuture) !== JSON.stringify(userMeetsFuture) ){
      this.userMeetsFuture = userMeetsFuture
    }
  }


  @action.bound
  setSelectedMyMeet(meet){
    this.selectedMyMeet = meet
  }

  getUserMeetsInPorgressInWaitPeriod(){
    const userMeetsInPorgressInWaitPeriod = this.user_meets.filter( (meet) => {
      return (
           isBeforeThanNow(meet.meet_time) //started
        && isAfterThanNow(meet.meet_time, this.meetWaitPeriod) //in wait period
        && meet.users.some( (user) => user.pivot.approve_membering === null ) //someone not approved
      )
    })

    let userMeetsInPorgressInWaitPeriodClone = cloneViaJson(userMeetsInPorgressInWaitPeriod)

    userMeetsInPorgressInWaitPeriodClone.map( (meet) => {
      meet.timeBeforeEndWaitPeriod = periodBetween(meet.meet_time, null, this.meetWaitPeriod)
      return meet
    })

    return userMeetsInPorgressInWaitPeriodClone
  }

  getUserMeetsInPorgress(){
    return this.user_meets.filter( (meet) => isBeforeThanNow(meet.meet_time) && isAfterThanNow(meet.meet_time, this.meetDuration) )
  }

  getUserMeetsPast(){
    return this.user_meets.filter( (meet) => isBeforeThanNow(meet.meet_time, this.meetDuration) )
  }

  getUserMeetsFuture(){
    return this.user_meets.filter( (meet) => isAfterThanNow(meet.meet_time) )
  }


  getCurrentPlace(){
    const google_place_id = this.current_meet_place.id
    const current_place = this.getMeetPlace(google_place_id)
    return current_place
  }


  getUserId(){
    const jwt = parse_jwt(this.jwtToken)
    let user_id = null
    if(jwt !== null && typeof jwt === "object" && 'sub' in jwt){
      user_id = jwt.sub 
    }
    this.setUserId(user_id)
    return this.user_id
  }


  @action.bound
  setUserId(user_id){
    this.user_id = user_id
  }


  @action.bound
  setCurrentMeetPlaceModalVisible(currentMeetPlaceModalVisible) {
    this.currentMeetPlaceModalVisible = currentMeetPlaceModalVisible
  }


  @action.bound
  setCurrentPlace(current_meet_place) {
    this.current_meet_place = current_meet_place
  }
  

  async getUserLocation() {
    let user_location = await Location.getCurrentPositionAsync({})
    this.setUserLocation(user_location)
    return this.user_location
  }


  @action.bound
  setUserLocation(user_location) {
    this.user_location = user_location
  }


  updateMeetGroupsTemplateTime(){
    const meetsTimeRes = meetsTime()
    
    for (i = 1; i <= Object.keys(meetsTimeRes).length; i++) {
      this.setPropsOfMeetGroup( 
        { 
          value: meetsTimeRes['meetGroup' + i],
          timeFormatUser: timeFormatUser(meetsTimeRes['meetGroup' + i]),
          timeFormatDB: timeFormatDB(meetsTimeRes['meetGroup' + i]),
          timeObject: cloneDeep(meetsTimeRes['meetGroup' + i]),
        },
        i,
        this.meetGroupsTemplate
      )
    }
  }

  initMeetPlaces(meet_places) {  
    
    this.updateMeetGroupsTemplateTime()

    const meet_places_addition_cleared = clearMeetPlacesAdditionDoubles(meet_places, toJS(this.meet_places_addition))
    meet_places = meet_places.concat(meet_places_addition_cleared)

    const meet_places_with_groups = meet_places.map( 
      (el) => {
        el.groups = cloneDeep(this.meetGroupsTemplate)
        return el
      } 
    )

    this.setMeetPlaces(meet_places_with_groups) 
  }


  setPropsOfMeetGroup(props, groupNumber, state) {
    for (let prop in props) {
      state['meetGroup' + groupNumber][prop] = props[prop]
    }
  }


  @action.bound
  setMeetPlaces(meet_places){
    this.meet_places = meet_places
  }


  @action.bound
  addMeetsDataToPlaces(meets){
    const meet_places = this.meet_places
    const user_id = this.user_id
    const meet_places_with_meets_data = []
    
    meet_places.map( 
      (place) => { 

        const meetscloneDeep =  cloneDeep(meets)
        const meetsOfPlace = meets.filter( 
          (meet) => meet.google_place_id === place.id
        )

        place.meets = meetsOfPlace
      }
    )
    
    //add data from meets to groups (manСount, womanСount, memberOfMeet)
    meet_places.map( 
      (place) => {
        const groupsLength = Object.keys(place.groups).length
        //set default (rewrite old data)
        for(i = 1; i <= groupsLength; i++){
          place.groups['meetGroup'+i].manСount = 0
          place.groups['meetGroup'+i].womanСount = 0
          place.groups['meetGroup'+i].memberOfMeet = false
        }

        //set actual
        place.meets.map( 
          (meet) => {

            let memberOfmeet

            for(i = 1; i <= groupsLength; i++){
              if(place.groups['meetGroup'+i].timeFormatDB + i + place.id === meet.composite_meet_id){

                place.groups['meetGroup'+i].manСount = meet.man_count
                place.groups['meetGroup'+i].womanСount = meet.woman_count

                memberOfMeet = meet.users.some( 
                  (user) => user.id === user_id
                )

                place.groups['meetGroup'+i].memberOfMeet = memberOfMeet
              }
            }

          } 
        )

      }
    )  
    
  } 


  getMeetPlace(google_place_id){
    return this.meet_places.find( (place) => place !== null && place.id === google_place_id )
  }


  @action.bound
  async saveSettings(sex: number, sex_need: number, birth_date: string, phone_number: string, user_name: string) {

    if(this.saveSettingsInProcess === false){
      this.saveSettingsInProcess = true

      let user_age = age(birth_date)
      
      let user_group = 0
      if(sex === sex_need && sex === 1){
        
        if(user_age < 19){
          user_group = 4
        }else if(user_age < 36 && user_age > 18){
          user_group = 5
        }else{
          user_group = 6
        }

      }else if(sex === sex_need && sex === 2){

        if(user_age < 19){
          user_group = 7
        }else if(user_age < 36 && user_age > 18){
          user_group = 8
        }else{
          user_group = 9
        }

      }else if(user_age < 19){
        user_group = 1
      }else if(user_age < 36 && user_age > 18){
        user_group = 2
      }else if(user_age > 35){
        user_group = 3
      }

      const response = await this.api.saveSettings(sex, sex_need, birth_date, user_group, phone_number, user_name)

      if(response !== undefined && response.status === 200){

        this.set({name: 'user_group', value: user_group})

        showMessage({
          message: "Сохранено",
          type: "success",
        })
      }

      this.saveSettingsInProcess = false
    }

  }


  @action.bound
  async orderMeet(meet_time: momentjs, user_group, marker){

    if(this.orderMeetInProcess === false){
      this.orderMeetInProcess = true

      const google_place_id = marker.id
      const google_place_id_for_query = marker.place_id
      const place_latitude = marker.geometry.location.lat
      const place_longitude = marker.geometry.location.lng
      console.log('marker: ', marker)
      //balance check
      
      if([1,2,3].includes(user_group)){

        const group = marker.groups['meetGroup'+user_group]

        if(this.sex === 1){
          const diff1 = group.manСount - group.womanСount
          if(diff1 >= 1){
            Alert.alert(
              'Балансировщик ожидает женщин. Попробуйте позже.', 
              'В данный момент в этой встрече мужчин больше чем женщин. Система ожидает пока на встречу зарегистрируется больше женщин. Как только это произойдёт вы сможете принять в ней участие.'
            )

            this.orderMeetInProcess = false
            return new Error('Balancer: man > woman')
          }
        }

        if(this.sex === 2){
          const diff2 = group.womanСount - group.manСount
          if(diff2 >= 1){
            Alert.alert(
              'Балансировщик ожидает мужчин. Попробуйте позже.', 
              'В данный момент в этой встрече женщин больше чем мужчин. Система ожидает пока на встречу зарегистрируется больше мужчин. Как только это произойдёт вы сможете принять в ней участие.'
            )

            this.orderMeetInProcess = false
            return new Error('Balancer: woman > man')
          }
        }      
      }


      const response = await this.api.orderMeet(
        meet_time,
        google_place_id,
        user_group,
        google_place_id_for_query,
        place_latitude,
        place_longitude,
      )
      
      //console.log('response: ', response)

      if(response !== undefined && response.status === 200){
        await this.getMeets()

        showMessage({
          message: "Вы участвуете во встрече",
          type: "success",
        })
      }else{
        showMessage({
          message: "Ошибка, попробуйте позже",
          type: "danger",
        })
      }

      this.orderMeetInProcess = false
    }
    
  }


  @action.bound
  async unorderMeet(meet_time: momentjs, user_group, marker){

    if(this.unorderMeetInProcess === false){
      this.unorderMeetInProcess = true

      const google_place_id = marker.id

      const response = await this.api.unorderMeet(meet_time, google_place_id, user_group)
      
      if(response !== undefined && response.status === 200){
        await this.getMeets()
        showMessage({
          message: "Вы больше не участвуете во встрече",
          type: "success",
        })
      }else{
        showMessage({
          message: "Ошибка, попробуйте позже",
          type: "danger",
        })
      }

      this.unorderMeetInProcess = false
    }
  }


  getMeet(composite_meet_id){
    return this.meets.filter(meet => meet.composite_meet_id === composite_meet_id)[0]
  }

  
  async addPlacesToUserMeets(){
    const user_meets = this.user_meets //for reaction of autorun
    const user_group = this.user_group //for reaction of autorun

    user_meets.map(
      async (meet, index, array) => {

        let place = this.getMeetPlace(meet.google_place_id)

        if(!place){
          place = await this.google.getPlace(meet.google_place_id_for_query)
        }

        this.setUserMeetPlace(index, place)
      }
    )
  }
  

  @action.bound
  setUserMeetPlace(index, place){
    this.user_meets[index].place = place
  }


  @action.bound
  async getMeets(onlyMyMeets = false, onlyMeetsForMeetPlaces = false){
    this.meetsLoading = true
    let response = null

    if(!onlyMyMeets && this.meet_places.length){
      const google_place_ids = this.meet_places.map( (place) => place.id )  

      const google_place_ids_json_string = JSON.stringify(google_place_ids)
      response = await this.api.getMeets(google_place_ids_json_string)
      
      if(response !== undefined && response.status === 200){
        
        if( Array.isArray(response.data) ){
          this.meets = response.data
        }else{
          this.meets = []
        }
        
      }else{
        showMessage({
          message: "Ошибка, попробуйте позже",
          type: "danger",
        })
      }
    }

    if(!onlyMeetsForMeetPlaces){
      response = await this.api.getMeets()

      if(response !== undefined && response.status === 200){
        this.user_meets = ( response.data || [] )
      }else{
        showMessage({
          message: "Ошибка, попробуйте позже",
          type: "danger",
        })
      }
    }

    this.meetsLoading = false

    return response
  }

  
  @action.bound
  async signIn(queryParams) {

    if(this.jwtToken){
      return console.log('Cant login (already logined).')
    }

    let {sex, sex_need, birth_date, user_group, phone_number, user_name, server_needs_device_time, token, error} = queryParams
     
    if(error){
      return Alert.alert('Ошибка', error)
    }

    if(token){
      phone_number = (phone_number) ? phone_number.trim() : phone_number
      user_name = (user_name) ? user_name.trim() : user_name
      server_needs_device_time = ( parseInt(server_needs_device_time) ) ? true : false

      this.set(
        {name: 'jwtToken', value: token },
        {name: 'sex', value: parseInt(sex) || 0 },
        {name: 'sex_need', value: parseInt(sex_need) || 0 },
        {name: 'birth_date', value: birth_date},
        {name: 'user_group', value: parseInt(user_group) },
        {name: 'phone_number', value: phone_number},
        {name: 'user_name', value: user_name},
        {name: 'server_needs_device_time', value: server_needs_device_time},
      )

      this.navigation.navigate("App")
      await this.api.registerForPushNotifications()

      //send device time for sync server and device
      await this.api.sendDiviceTimeToServer()
    }else{
      return Alert.alert('Ошибка', 'Неудачный вход в аккаунт (не передан токен). Повторите попытку позже.')
    }
  }


  async signOut() {
    await this.clear()
    this.navigation.navigate("Auth")
  }
  

  @action.bound
  setJwtToken(jwtToken) {
    this.jwtToken = jwtToken
  }

  navigate(name = "App") {
    this.navigation.navigate(name)
  }

  @action.bound
  async clear() {

    return this.set(
      {name: 'user_id', value: null},
      {name: 'jwtToken', value: ''},
      {name: 'sex', value: 0},
      {name: 'sex_need', value: 0},
      {name: 'birth_date', value: ''},
      {name: 'user_name', value: ''},
      {name: 'phone_number', value: '+7'},
      {name: 'user_group', value: 0},
      {name: 'meets', value: []},
      {name: 'meet_places', value: []},
      {name: 'meet_places_addition', value: []},      
      {name: 'current_meet_place', value: {}},
      {name: 'user_location', value: {coords: { latitude: 55.75775, longitude: 37.61708}} },
      {name: 'currentMeetPlaceModalVisible', value: false},
      {name: 'orderMeetInProcess', value: false},
      {name: 'unorderMeetInProcess', value: false},
      {name: 'saveSettingsInProcess', value: false},
      {name: 'user_meets', value: []},
      {name: 'meetsLoading', value: false},
      {name: 'selectedMyMeet', value: false},
      {name: 'userMeetsInPorgressInWaitPeriod', value: []},
      {name: 'userMeetsInPorgress', value: []},
      {name: 'userMeetsPast', value: []},
      {name: 'userMeetsFuture', value: []},
      {name: 'meetsRealStartTime', value: []},
      {name: 'lastPushNotification', value: []},
      {name: 'expo_push_token_registered', value: false},
      {name: 'server_needs_device_time', value: false},
      {name: 'userMeetsFuture', value: []},
      {name: 'user_latitude_at_server', value: 0},
      {name: 'user_longitude_at_server', value: 0},
      {name: 'push_initiated_popup', value: {}},
      {name: 'push_initiated_popup_google_place_id_for_query', value: null},
      //{name: 'meetPlacesByCoordsCache', value: []},//clear meetPlaces cache when logout
    )
    
  }
}

export default new MainState()