import mainState from './mainState'
import { AsyncTrunk, SyncTrunk, ignore } from 'mobx-sync'
import { AsyncStorage } from 'react-native'
import navigationStore from './navigationStore'
import { getLocationDataAsync } from './../../helpers/requests'


const initPersistStateAsync = async function(store){

	/**
	* @desc create an async trunk with custom options
	* @type {AsyncTrunk}
	*/
	const trunk = await new AsyncTrunk(store, {

		/**
		* @desc custom storage: built in storage is supported
		*  - localStorage
		*  - sessionStorage
		*  - ReactNative.AsyncStorage
		*/
		storage: AsyncStorage,

		/**
		* @desc custom storage key, the default is `__mobx_sync__`
		*/
		storageKey: '__persist_mobx_stores__',

		/**
		* @desc the delay time, use for mobx reaction
		*/
		delay: 1e3,
	})


	/**
	* @desc load persisted stores
	*/
	await trunk.init().then( async () => {
		/**
		* @desc do any staff with the loaded store,
		* and any changes now will be persisted
		* @type {boolean}
		*/
		store.storeLoaded = true
		console.log('Mobx store loaded from device storage.')
	})

}


const internal = {
	initPersistState: initPersistStateAsync,
	
	@ignore
	storeLoaded: false,

	mainState: mainState,
	navigationStore: navigationStore,
	// place for other stores...
}

export default internal