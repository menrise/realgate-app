import React from 'react'
import { Platform, StatusBar, StyleSheet, View} from 'react-native'
import { AppLoading, Asset, Font, Icon } from 'expo'
import AppNavigator from './navigation/AppNavigator'
import { Provider, observer } from 'mobx-react/native'
import store from './stores/mobx'
import FlashMessage from 'react-native-flash-message'
import ContentContainer from './screens/ContentContainer'
import Sentry from 'sentry-expo'
import { FontAwesome } from '@expo/vector-icons'
import { testAsyncStorage } from './helpers/misc'


//Sentry.enableInExpoDevelopment = true
const sentryDSN = 'https://afd9e452a49a47aab428c729f0d723a4@sentry.io/1369684'
Sentry.config(sentryDSN).install()

//import Reactotron from 'reactotron-react-native'
//Reactotron.configure({ host: '192.168.1.3', port: 9090, }).useReactNative().connect()


@observer
export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  }

  async componentWillMount(){
    //not use skipLoadingScreen because it may cause s.getChildNavigation(t.key) is undefined error
    if(this.props.skipLoadingScreen){
      await store.initPersistState(store)
      testAsyncStorage()      
    }
  }


  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <Provider mainState={store.mainState} rootNavigation={store.navigationStore}>
          <ContentContainer/>
        </Provider>
      )
    }
  }


  _loadResourcesAsync = async () => {
    await store.initPersistState(store)
    testAsyncStorage()

    await Promise.all([
      Asset.loadAsync([
        require('./assets/images/realgate-icon-without-name-1024x1024-4.png'),
        require('./assets/images/realgate-icon-big-1024x1024-without-name-transparent-7.png'),
        require('./assets/images/realgate-splash-hi-1.png'),
        require('./assets/images/mall.png'),
      ]),
      Font.loadAsync(FontAwesome.font),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Icon.Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      }),
    ])
    
  }

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error)
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
})
