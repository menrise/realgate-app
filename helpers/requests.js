import mainState from './../stores/mobx/mainState'
import { Permissions, Linking } from 'expo'
import { Alert } from 'react-native'
import { makeId } from './misc'


export let getLocationDataAsync = async () => {
	try {
		await mainState.getUserLocation()

		const user_latitude = mainState.user_location.coords.latitude
		const user_longitude = mainState.user_location.coords.longitude

		await mainState.google.getMeetPlaces(user_latitude, user_longitude)

		await mainState.getMeets(false, true)
		mainState.api.sendUserCoordinates(user_latitude, user_longitude)
		
	} catch (error) {
	  	console.log(error)
	}
}


export let regularMeetsUpdateFromServer = async () => {
	try {
		if(mainState.jwtToken){
			mainState.getMeets(true)

			let { status } = await Permissions.getAsync(Permissions.LOCATION)
			if(status === 'granted'){
				mainState.getMeets(false, true)
			}
			console.log('Meets update from server.')
		}
	} catch (error) {
	  	console.log(error)
	}
}

//redirect gotten from backend after auth
export const handleAuthRedirect = async function(event) {

	let authRedirectToAppFlag = false
	
	if( event.url.includes('auth_redirect_to_app') ){
		authRedirectToAppFlag = true
	}else if( event.url.includes('+expo-auth-session') ){
		authRedirectToAppFlag = true
	}

	if(!authRedirectToAppFlag) {
		return false
	}

	const urlWithoutUrlHash = event.url.split('#')[0]
	let data = Linking.parse(urlWithoutUrlHash)

	await mainState.signIn(data.queryParams) //jwt token from my backend 
}

export const getDeepLink = function (additionPath){
	//uniq_id - fix error in v51 chrome (second login not redirected to backend api, mb cache same url)
	//auth_redirect_to_app - for recognition auth redirect to app (in url Listener)
	const uniqBasepathLandpageAfterAuth = 'auth_redirect_to_app/uniq_id-' + makeId(10) 

	path = uniqBasepathLandpageAfterAuth

	if(additionPath){
		path = path + '/' + additionPath
	}

	return Linking.makeUrl(path)
}



export default {
	getLocationDataAsync: getLocationDataAsync,
	regularMeetsUpdateFromServer: regularMeetsUpdateFromServer,
	handleAuthRedirect: handleAuthRedirect,
	getDeepLink: getDeepLink,
}
