import { timeFormatDB } from './time'


export const composite_meet_id = function (meet_time, user_group, google_place_id){
	return timeFormatDB(meet_time) + user_group + google_place_id
}


export default {
	composite_meet_id: composite_meet_id,
}
