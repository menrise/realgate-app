import moment from 'moment-timezone-all'
import 'moment/locale/ru'
import { gapBetweenLastApproveAndRealMeetStart } from './../constants/System'

moment.tz.setDefault("Europe/Moscow")
moment.locale('ru')


export const getTimestamp = function (time){
  return moment(time).format('X')
}

export const getRealTimeStart = function (user_device_time, serverTime, users){

  const deltaUserVsServerSeconds = periodBetween(user_device_time, serverTime, 0, true)

  const timestampsOfUserApproves = users.map( (user) => {
    if(user.pivot.approve_membering_at) {
      return getTimestamp(user.pivot.approve_membering_at)
    }
  }).filter( (element) => {
    if(element) return true
  })

  const latestApprove = Math.max.apply(null, timestampsOfUserApproves)
  const realStartTimestamp = latestApprove + deltaUserVsServerSeconds + gapBetweenLastApproveAndRealMeetStart
  const realStartTimestampFormWithMillSeconds = parseInt(realStartTimestamp.toString() + '000')
  const realStartTime = timeFormatDB( realStartTimestampFormWithMillSeconds )

  return realStartTime
}



export const toMinSecFormat = function (seconds){
  const minSecArr = (seconds / 60).toString().split('.')
  const secondsProportion = Number('0.' + minSecArr[1])
  seconds = (secondsProportion) ? Math.round( secondsProportion * 60 ) : 0
  seconds = seconds.toString().padStart(2, "0")
  return minSecArr[0] + ':' + seconds //to mm:ss format
}

export const periodBetween = function (time1, time2 = moment(), minutesGap = 0, returnSeconds = false){
  if(!time2) time2 = moment()
  
  const timeMoment = moment( time1 )
  const timeEndOfWaitPeriod = timeMoment.add(minutesGap, 'minute')
  const diff = timeEndOfWaitPeriod.diff(time2, 'seconds')

  if(!returnSeconds){
    return toMinSecFormat(diff)
  }

  return diff
}

export const getRoundInfo = function(secondsAfterMeetRealStart, meetRoundPeriod){

  if(secondsAfterMeetRealStart === null){
    return {round: '', secondAfterStartCurrentRound: '', timeFromRoundStart: '', secondsBeforeRoundEnd: '', timeBeforeRoundEnd: '', roundIndex: 0}
  }

  const div = secondsAfterMeetRealStart / meetRoundPeriod
  const divString = div.toString()
  const divArr = divString.split('.')
  const fullMeetRoundPeriodsSeconds = divArr[0] * meetRoundPeriod

  //console.log('divArr: ', divArr)
  const round = parseInt(divArr[0]) + 1
  const roundIndex = round - 1
  const secondAfterStartCurrentRound = secondsAfterMeetRealStart - fullMeetRoundPeriodsSeconds
  //console.log('secondAfterStartCurrentRound: ', secondAfterStartCurrentRound)
  const timeFromRoundStart = toMinSecFormat(secondAfterStartCurrentRound)
  const secondsBeforeRoundEnd = meetRoundPeriod - secondAfterStartCurrentRound
  const timeBeforeRoundEnd = toMinSecFormat(secondsBeforeRoundEnd)

  return {round: round, roundIndex: roundIndex, secondAfterStartCurrentRound: secondAfterStartCurrentRound,
    timeFromRoundStart: timeFromRoundStart, secondsBeforeRoundEnd: secondsBeforeRoundEnd,
    timeBeforeRoundEnd: timeBeforeRoundEnd}
}

export const isBeforeThanNow = function (time, minutesGap){

  const timeAfter = moment()
  const timeMoment = moment( time )
  
  if(minutesGap){
    timeMoment.add(minutesGap, 'minute')
  }

  return timeMoment.isBefore( timeAfter )
}

export const isAfterThanNow = function (time, minutesGap){

  const timeBefore = moment()
  const timeMoment = moment( time )
  
  if(minutesGap){
    timeMoment.add(minutesGap, 'minute')
  }

  return timeMoment.isAfter( timeBefore )
}

export const age = function (birth_date){
 return moment().diff(birth_date, 'years', false)
}

export const maxBirthDate = function () {
  return moment().add(-18, 'year').format("YYYY-MM-DD").toString()
}

export const meetsTime = function (userGroup){

  //many pairs
  let meetGroup1 = moment().weekday(4).hours(19).startOf('hour')
  meetGroup1 = correctMeetTime(meetGroup1)

  let meetGroup2 = moment().weekday(5).hours(19).startOf('hour')
  meetGroup2 = correctMeetTime(meetGroup2)

  let meetGroup3 = moment().weekday(6).hours(19).startOf('hour')
  meetGroup3 = correctMeetTime(meetGroup3)

  let meetGroup4 = moment().weekday(0).hours(19).startOf('hour')
  meetGroup4 = correctMeetTime(meetGroup4)

  let meetGroup5 = moment().weekday(0).hours(20).startOf('hour')
  meetGroup5 = correctMeetTime(meetGroup5)

  let meetGroup6 = moment().weekday(0).hours(21).startOf('hour')
  meetGroup6 = correctMeetTime(meetGroup6)

  let meetGroup7 = moment().weekday(1).hours(19).startOf('hour')
  meetGroup7 = correctMeetTime(meetGroup7)

  let meetGroup8 = moment().weekday(1).hours(20).startOf('hour')
  meetGroup8 = correctMeetTime(meetGroup8)

  let meetGroup9 = moment().weekday(1).hours(21).startOf('hour')
  meetGroup9 = correctMeetTime(meetGroup9)

  //1vs1
  let meetGroup10 = moment().weekday(4).hours(20).startOf('hour')
  meetGroup10 = correctMeetTime(meetGroup10)

  let meetGroup11 = moment().weekday(5).hours(20).startOf('hour')
  meetGroup11 = correctMeetTime(meetGroup11)

  let meetGroup12 = moment().weekday(6).hours(20).startOf('hour')
  meetGroup12 = correctMeetTime(meetGroup12)

  let meetGroup13 = moment().weekday(0).hours(16).startOf('hour')
  meetGroup13 = correctMeetTime(meetGroup13)

  let meetGroup14 = moment().weekday(0).hours(17).startOf('hour')
  meetGroup14 = correctMeetTime(meetGroup14)

  let meetGroup15 = moment().weekday(0).hours(18).startOf('hour')
  meetGroup15 = correctMeetTime(meetGroup15)

  let meetGroup16 = moment().weekday(1).hours(16).startOf('hour')
  meetGroup16 = correctMeetTime(meetGroup16)

  let meetGroup17 = moment().weekday(1).hours(17).startOf('hour')
  meetGroup17 = correctMeetTime(meetGroup17)

  let meetGroup18 = moment().weekday(1).hours(18).startOf('hour')
  meetGroup18 = correctMeetTime(meetGroup18)

  let times = {
    meetGroup1: meetGroup1,
    meetGroup2: meetGroup2,
    meetGroup3: meetGroup3,
    meetGroup4: meetGroup4,
    meetGroup5: meetGroup5,
    meetGroup6: meetGroup6,
    meetGroup7: meetGroup7,
    meetGroup8: meetGroup8,
    meetGroup9: meetGroup9,
    meetGroup10: meetGroup10,
    meetGroup11: meetGroup11,
    meetGroup12: meetGroup12,
    meetGroup13: meetGroup13,
    meetGroup14: meetGroup14,
    meetGroup15: meetGroup15,
    meetGroup16: meetGroup16,
    meetGroup17: meetGroup17,
    meetGroup18: meetGroup18,    
  }

  if(userGroup === undefined){
    return times
  }else{
    return times['meetGroup' + parseInt(userGroup)]
  }

}


function correctMeetTime(meetTime){

  let nowPlusPerioud = moment().add(1, 'hours')

  if(meetTime.isBefore(nowPlusPerioud)){
    meetTime = meetTime.add(7, 'days')
  }

  return meetTime
}


export const timeFormatUser = function(momentInstance){
  if(momentInstance instanceof moment){
    return momentInstance.format('dddd D MMM, HH:mm')
  }else if(momentInstance){
    return moment(momentInstance).format('dddd D MMM, HH:mm')
  }
  else{
    return momentInstance
  }
}


export const timeFormatDB = function(momentInstance){
  if(momentInstance instanceof moment){
    return momentInstance.format('YYYY-MM-DD HH:mm:ss')
  }else if(momentInstance){
    return moment(momentInstance).format('YYYY-MM-DD HH:mm:ss')
  }
  else{
    return momentInstance
  }
}

export const nowMoment = function(){
    const nowMoment = moment()
    return nowMoment
}


export default {
  age: age,
  meetsTime: meetsTime,
  timeFormatUser: timeFormatUser,
  timeFormatDB: timeFormatDB,
  isBeforeThanNow: isBeforeThanNow,
  periodBetween: periodBetween,
  nowMoment: nowMoment,
  toMinSecFormat: toMinSecFormat,
  getRoundInfo: getRoundInfo,
  getTimestamp: getTimestamp,
  getRealTimeStart: getRealTimeStart,
}
