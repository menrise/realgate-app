
export const getSet = function(target, property, descriptor) {

    return {
        set: function (value) {
            this[property] = value
            //console.log('set: ' + property + ' = ', value)
        },
        get: function() {
            //console.log('get: ' + property + ' = ', this['_'+property])
            return this[property]
        },
        enumerable: true,
        configurable: true
    }
}


export default {
	getSet: getSet,
}
