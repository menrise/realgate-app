
export let playSound = function (){

	let sounds = {}
	return async function (soundName) {
		let source
		if(soundName === 'roundChange'){
			source = require('./../assets/sounds/jingle-bell.mp3')
		}
		
		if(!source) return false

		if(!sounds[soundName]){
			sounds[soundName] = new Expo.Audio.Sound()

			try {
				await sounds[soundName].loadAsync(source)
			} catch (error) {
				console.log(error)
			}
		}

		try {
		  await sounds[soundName].replayAsync()
		} catch (error) {
		  console.log(error)
		}
	}
}()


export default {
	playSound: playSound,
}
