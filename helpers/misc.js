import { composite_meet_id } from './names'
import Base64 from 'base-64'
import Utf8 from 'utf8'
import { Alert, AsyncStorage } from 'react-native'


export const man_count = function (state, meet_time, user_group, google_place_id){
    const composite_meet_id_res = composite_meet_id(meet_time, user_group, google_place_id)
    const man_count = (state.meets[composite_meet_id_res]) ? state.meets[composite_meet_id_res].man_count : 0
    return man_count
}

export const woman_count = function (state, meet_time, user_group, google_place_id){
    const composite_meet_id_res = composite_meet_id(meet_time, user_group, google_place_id)
    const woman_count = (state.meets[composite_meet_id_res]) ? state.meets[composite_meet_id_res].woman_count : 0
    return woman_count
}

export const parse_jwt = function (token) {
    const base64Url = token.split('.')[1]
    if(typeof base64Url !== "string" ) return null
    	
    const base64Encoded = base64Url.replace(/-/g, '+').replace(/_/g, '/')
	const bytes = Base64.decode(base64Encoded);
	const json = Utf8.decode(bytes);
    return JSON.parse(json)
}

export const memberOfMeet = function (timeFormatDB, user_group, google_place_id, user_id, state) {
	const composite_meet_id = timeFormatDB+user_group+google_place_id
	
	if(typeof state.meets !== "object" ) return false

	if(typeof state.meets[composite_meet_id] !== "object" ) return false
		
	const userInMeet = state.meets[composite_meet_id].users.filter(user => user.id === user_id)

	if(userInMeet.length > 0) return true

	return false
}


export const makeId = function (length = 10) {
  return [...Array(length)].map(i=>(~~(Math.random()*36)).toString(36)).join('')
}

export const cloneViaJson = function (origin) {
  return JSON.parse( JSON.stringify(origin) )
}

export const calcApproveDecides = function (meet){
  let mansApprove = []
  let womansApprove = []
  let mansUnapprove = []
  let womansUnapprove = []
  let mans = []
  let womans = []

  meet.users.map( (user) => {
    
    if(user.pivot.sex === 1){
      mans.push(user)

      if(user.pivot.approve_membering === 1) mansApprove.push(user)

      else if(user.pivot.approve_membering === 2) mansUnapprove.push(user)

    }else if(user.pivot.sex === 2){
      womans.push(user)

      if(user.pivot.approve_membering === 1) womansApprove.push(user)
        
      else if(user.pivot.approve_membering === 2) womansUnapprove.push(user)

    }
  })


  const isMultisex = (mans.length > 0 && womans.length > 0) ? true : false
  const isMonosexMan = (!isMultisex && mans.length > 0) ? true : false
  const isMonosexWoman = (!isMultisex && womans.length > 0) ? true : false

  const decides = {mans: mans, womans: womans, mansApprove: mansApprove,
    womansApprove: womansApprove, mansUnapprove: mansUnapprove,
    womansUnapprove: womansUnapprove, isMultisex: isMultisex,
    isMonosexMan: isMonosexMan, isMonosexWoman: isMonosexWoman}

  return decides
}

export const canMeetStart = function(decides, meetsInPorgress, waitedMeets){
  let canMeetStart = true 

  //wait period end
  if (meetsInPorgress.length && !waitedMeets.length) {
    
    //can start when at least one of participating man or woman approved membering
    if(decides.mans.length > 0 
      && (
            (decides.mansApprove.length === 0) //not one approved (mix 1 man in multisex meet)
        ||  (decides.mansApprove.length < 2 && !decides.womans.length) //approved less when 2 when have not womans (min 2 members in monosex meet)
      )
    ){
      canMeetStart = false
    }

    if(decides.womans.length > 0 
      && (
            (decides.womansApprove.length === 0) //not one approved (mix 1 woman in multisex meet)
        ||  (decides.womansApprove.length < 2 && !decides.mans.length) //approved less when 2 when have not mans (min 2 members in monosex meet)
      )
    ){
      canMeetStart = false
    }

    return canMeetStart

  //wait period not end yet
  }else if (meetsInPorgress.length && waitedMeets.length) {

    if(decides.mans.length > 0 //if have mans partiocipants
      && (
           (decides.mansApprove.length + decides.mansUnapprove.length < decides.mans.length) //not all decided 
        || (decides.mansApprove.length === 0) //not one approved (mix 1 man in multisex meet)
        || (decides.mansApprove.length < 2 && !decides.womans.length) //approved less when 2 when have not womans (min 2 members in monosex meet)
      )
    ){
      canMeetStart = false
    }


    if(decides.womans.length > 0 //if have womans partiocipants
      && (
           (decides.womansApprove.length + decides.womansUnapprove.length < decides.womans.length) //not all decided 
        || (decides.womansApprove.length === 0) //not one approved (mix 1 woman in multisex meet)
        || (decides.womansApprove.length < 2 && !decides.mans.length) //approved less when 2 when have not womans (min 2 members in monosex meet)
      )
    ){
      canMeetStart = false
    }

    return canMeetStart
  }

  return false
  
}


export const getUserById = function(users, id){
    const user = users.find( (user) => {
      return user.id === id
    })

    return user
}


export const generateMeetRounds = function(decidesOrigin){

  //unlink arrays but save links to users
  let decides = {}
  for (let prop in decidesOrigin) {
    if( Array.isArray(decidesOrigin[prop]) ){
      decides[prop] = decidesOrigin[prop].map( (user) => user )
    }else{
      decides[prop] = decidesOrigin[prop]
    }
  }


  let rounds = []
  let pairs = []

  if(decides.isMultisex){

    let moreMembersSex 
    if(decides.mansApprove.length > decides.womansApprove.length){
      moreMembersSex = decides.mansApprove
      lessMembersSex = decides.womansApprove
    }else{
      moreMembersSex = decides.womansApprove
      lessMembersSex = decides.mansApprove
    }

    let round = []
    const length = moreMembersSex.length
    for(let i = 0 ; i < length ; i++ ){
      round = lessMembersSex.map( (member1, index, array) => {
        return {member1: lessMembersSex[index], member2: moreMembersSex[index]}
      })
      rounds.push(round)
      //rotation of array
      moreMembersSex.unshift(moreMembersSex.pop())
    }

  }else{

    let members = (decides.isMonosexMan) ? decides.mansApprove : decides.womansApprove

    const membersLength = members.length
    const membersBackup = members

    if(membersLength > 2){

      const remainderDiv = membersLength % 2

      if(!remainderDiv) {//if even members.length

        //we need to create rounds like this (6 members monosex)
        /*
        round 1
        1 6 - pair
        2 5
        3 4
        round 2
        1 2
        3 6
        4 5
        round 3
        1 3
        4 2
        5 6
        round 4
        1 4
        5 3
        6 2
        round 5
        1 5
        6 4
        2 3
        */

        //generate arrays from 
        /*
        members
        1 
        2 
        3 
        4
        5
        6

        to 

        firstMember
        1

        firstMembersPartAfterFirstMember
        2
        3

        secondMembersPart
        4
        5
        6

        secondMembersPartReversed
        6
        5
        4

        at all we have
        1 6 - pair
        2 5 - pair
        3 4 - pair

        in rounds we routate it without first member like this 
        1 2
        3 6
        4 5
        */
        const membersAfterFirst = members.splice(1, membersLength)
        const firstMember = members
        const midMemberIndex = Math.floor(membersAfterFirst.length / 2)
        const firstMembersPartAfterFirstMember = membersAfterFirst.splice(0, midMemberIndex)
        const secondMembersPart = membersAfterFirst
        const secondMembersPartReversed = secondMembersPart.reverse()
        const loopLimit = membersLength - 1

        for(let outerI = 0 ; outerI < loopLimit ; outerI++ ){

          firstMembersPart = firstMember.concat(firstMembersPartAfterFirstMember)

          let round = []
          const firstMembersPartLength = firstMembersPart.length
          for(let i = 0 ; i < firstMembersPartLength ; i++ ){
            round.push( {member1: firstMembersPart[i], member2: secondMembersPartReversed[i]} )
          }

          rounds.push(round)

          //rotation of part arrays (without first member)
          secondMembersPartReversed.unshift(firstMembersPartAfterFirstMember.shift())
          firstMembersPartAfterFirstMember.push(secondMembersPartReversed.pop())
        }

      }else{//if not even members.length

        //we need to create rounds like this (5 members monosex)
        /*
        round
        1 5 - pair
        2 4
        3 

        2 1
        3 5
        4 

        3 2
        4 1
        5 

        4 3
        5 2
        1 

        5 4
        1 3
        2 
        */

        const midMemberIndex = Math.ceil(members.length / 2)
        const firstMembersPart = members.splice(0, midMemberIndex)
        const secondMembersPart = members
        const secondMembersPartReversed = secondMembersPart.reverse()
        const loopLimit = membersLength

        for(let outerI = 0 ; outerI < loopLimit ; outerI++ ){

          let round = []
          const firstMembersPartLength = firstMembersPart.length
          for(let i = 0 ; i < firstMembersPartLength ; i++ ){
            round.push( {member1: firstMembersPart[i], member2: secondMembersPartReversed[i]} )
          }

          rounds.push(round)

          //rotation of part arrays (without first member)
          secondMembersPartReversed.unshift(firstMembersPart.shift())
          firstMembersPart.push(secondMembersPartReversed.pop())
        }
      }
      
    }else{
      rounds.push( [{member1: members[0], member2: members[1]}] )
    }

  }

  return rounds
}

export const getUser = function(users, userId){
  if(!users) return null

  let neededUser = null
  users.some( (user) => {
    if(user.id === userId){
      neededUser = user
      return true
    }
  })

  return neededUser
}

export const testAsyncStorage = async function(){

  const itemName = '@testAsyncStorage:test1'
  const itemValue = 'ok!'
  const alertDesc = 'У вас недостаточно свободного места. Приложение не может работать нормально. Пожалуйста освободите больше места.'
  const alertTitle = 'Ошибка'

  //set test
  try {
    await AsyncStorage.setItem(itemName, itemValue)
  } catch (error) {
    console.error(error)
    Alert.alert(alertTitle, alertDesc)
    return
  }

  //get test
  try {
    const value = await AsyncStorage.getItem(itemName)
    if (value !== itemValue) {
      console.error('AsyncStorage set item value !== gotten item value. Set value is: ', itemValue, '. Gotten value is: ', value)
      Alert.alert(alertTitle, alertDesc)
    }
  } catch (error) {
    console.log(error)
    Alert.alert(alertTitle, alertDesc)
    return
  }

  //remove test
  try {
    await AsyncStorage.removeItem(itemName)
  } catch (error) {
    console.error(error)
    Alert.alert(alertTitle, alertDesc)
    return
  }

  console.log('Test AsyncStorage is OK.')

}

//clear doubles in meet_places_addition with meet_places
export const clearMeetPlacesAdditionDoubles = function  (meet_places, meet_places_addition){

  meet_places_addition_cleared = meet_places_addition.filter( (place_addition) => {

    return meet_places.every((place) => {

      if(place.id === place_addition.id){
        return false
      }
      return true
    })   

  })
  return meet_places_addition_cleared
}

export const getRandomInt = function  (min, max){
  return Math.floor(Math.random() * (max - min + 1)) + min
}


export default {
  man_count: man_count,
  woman_count: woman_count,
  parse_jwt: parse_jwt,
  memberOfMeet: memberOfMeet,
  makeId: makeId,
  cloneViaJson: cloneViaJson,
  calcApproveDecides: calcApproveDecides,
  canMeetStart: canMeetStart,
  getUserById: getUserById,
  generateMeetRounds: generateMeetRounds,
  getUser: getUser,
  testAsyncStorage: testAsyncStorage,
  clearMeetPlacesAdditionDoubles: clearMeetPlacesAdditionDoubles,
  getRandomInt: getRandomInt,
}
