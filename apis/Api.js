import axios from 'axios'
import { backendUrl } from './../constants/Apis'
import { defaults, get } from 'lodash'
import { Alert } from 'react-native'
import mainState from './../stores/mobx/mainState'
import { showMessage } from 'react-native-flash-message'
import { nowMoment, timeFormatDB } from './../helpers/time'
import { Permissions, Notifications } from 'expo'
import { userLatitudeDiapasonTrigerToSendNewToServer, userLongitudeDiapasonTrigerToSendNewToServer } from './../constants/Apis'


//configure axios instance
let request = axios.create({
  baseURL: backendUrl + 'api/',
  timeout: 10000,
})
request.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded'
request.defaults.headers.common['Accept'] = 'application/json'
request.interceptors.request.use(function(config) {

	if ( mainState.jwtToken ) {
		config.headers.Authorization = `Bearer ${mainState.jwtToken}`;
	}else{
		console.log('Jwt is empty!')
	}

  	return config;
}, function(err) {
  	return Promise.reject(err);
})
request.interceptors.response.use(
	function (response) {
		let newtoken = get(response, 'headers.authorization')
		
		if (newtoken) {
			mainState.setJwtToken( newtoken.replace('Bearer ', '') )
		}


		if( Array.isArray(response.data) ){
			//add user_device_time (this need for calculate delta with server time and based on it calculate meetsRealStartTime)
			const nowMomentTime = nowMoment()
			const data = response.data.map( (element) => {
				element.user_device_time = nowMomentTime
				return element
			})
			response.data = data
		}

		return response
	}, function (error) {

		if(error && error.response){
			switch (error.response.status) {
			  	case 401:
			  		console.log('401 error. signOut.')
			  		mainState.signOut()		    	
			    break
			  	default:
			    	console.log(error.response) //issue "There was a problem sending log messages to your development environment" in Expo app env
			}
		}

		return Promise.reject(error)
	}
)


function error(err){
	console.log('err: ', err)
	
    showMessage({
      message: "Ошибка, данные не были сохранены на сервере, попробуйте позже",
      type: "danger",
    })		
}


export default class Api {


	async saveSettings (sex, sex_need, birth_date, user_group, phone_number, name) {

		return await request.put('users', {
			sex: sex,
			sex_need: sex_need,
			birth_date: birth_date, 
			user_group: user_group, 
			phone_number: phone_number,
			name: name,
		}).catch(function(err) {   
			error(err)
		})
	}


	async orderMeet (
		meet_time,
		google_place_id,
		user_group,
		google_place_id_for_query,
		place_latitude,
		place_longitude
	) {

	    return await request.put('meets', {
		    meet_time: meet_time,
		    google_place_id: google_place_id,
		    google_place_id_for_query: google_place_id_for_query,
		    user_group: user_group, 
		    place_latitude: place_latitude,
		    place_longitude: place_longitude,
		    action: 'order',
	    }).catch(function(err) {
			error(err)
	    })
	}

	async unorderMeet (meet_time, google_place_id, user_group) {

	    return await request.put('meets', {
		    meet_time: meet_time,
		    google_place_id: google_place_id,
		    user_group: user_group, 
		    action: 'unorder',
	    }).catch(function(err) {
			error(err)
	    })
	}

	async getMeets(google_place_ids_json_string){

		if(google_place_ids_json_string === null){
		    return await request.post('meets', 
		    	{}
		    ).catch(function(err) {      
		    	error(err)
		    })			
		}

	    return await request.post('meets', {
	      	google_place_ids: google_place_ids_json_string,
	    }).catch(function(err) {      
	    	error(err)
	    })
  	}

  	async approveMembering (meet_id) {

	    return await request.put('meets', {
		    meet_id: meet_id,
		    action: 'approve_membering',
	    }).catch(function(err) {
			error(err)
	    })
	}

  	async unapproveMembering (meet_id) {

	    return await request.put('meets', {
		    meet_id: meet_id,
		    action: 'unapprove_membering',
	    }).catch(function(err) {
			error(err)
	    })
	}

	async like (meet_id, liked_user_id) {

	    return await request.put('likes', {
		    meet_id: meet_id,
		    liked_user_id: liked_user_id,
	    }).catch(function(err) {
			error(err)
	    })
	}

	async sendUserCoordinates (latitude, longitude) {
		const user_latitude_at_server = mainState.user_latitude_at_server
		const user_longitude_at_server = mainState.user_longitude_at_server
		//send if user went out triger coordinates diapason
		if(
			   !user_latitude_at_server
			|| !user_longitude_at_server
			|| latitude > user_latitude_at_server + userLatitudeDiapasonTrigerToSendNewToServer
			|| latitude < user_latitude_at_server - userLatitudeDiapasonTrigerToSendNewToServer
			|| longitude > user_longitude_at_server + userLongitudeDiapasonTrigerToSendNewToServer
			|| longitude < user_longitude_at_server - userLongitudeDiapasonTrigerToSendNewToServer
		){
		    return await request.put('users/coordinates', {
			    latitude: latitude,
			    longitude: longitude,
		    }).then(function (response) {
		    	console.log('User coordinates sended to server.')
				mainState.set({name: 'user_latitude_at_server', value: latitude})
				mainState.set({name: 'user_longitude_at_server', value: longitude})
			}).catch(function(err) {
				error(err)
		    })
		}

		return false
	}

	async sendDiviceTimeToServer () {

		if(mainState.server_needs_device_time){
		    return await request.put('users/device_time', {
			    device_time: timeFormatDB( nowMoment() ),
		    }).then(function (response) {
				mainState.set({name: 'server_needs_device_time', value: false})
			}).catch(function(err) {
				error(err)
		    })
		}
	}

	async registerForPushNotifications() {
		const { status: existingStatus } = await Permissions.getAsync(
			Permissions.NOTIFICATIONS
		)
		let finalStatus = existingStatus
		// only ask if permissions have not already been determined, because
		// iOS won't necessarily prompt the user a second time.
		if (existingStatus !== 'granted') {
			// Android remote notification permissions are granted during the app
			// install, so this will only ask on iOS
			const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)
			finalStatus = status
		}

		// Stop here if the user did not grant permissions
		if (finalStatus !== 'granted') {
			return
		}

		if(!mainState.expo_push_token_registered){

			// Get the token that uniquely identifies this device
			token = await Notifications.getExpoPushTokenAsync()

			// POST the token to your backend server from where you can retrieve it to send push notifications.
			return await request.post('exponent/devices/subscribe', {
			    expo_token: token,
			}).then(function (response) {
				mainState.set({name: 'expo_push_token_registered', value: true})
			}).catch(function(err) {
				error(err)
			})
		}

		
	}
}