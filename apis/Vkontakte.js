import { AuthSession, Linking } from 'expo'
import { backendUrl, vkontakteAppId } from './../constants/Apis'
import mainState from './../stores/mobx/mainState'
import { handleAuthRedirect, getDeepLink } from '../helpers/requests'

export default class Facebook {


	async auth () {
		//let redirectUrlApp = AuthSession.getRedirectUrl()
		let redirectUrlMyBackend = backendUrl + 'api/auth/vkontakte/callback'
		/*
		let result = await AuthSession.startAsync({
			authUrl:
			`https://oauth.vk.com/authorize?response_type=code` +
			`&client_id=${vkontakteAppId}` +
			`&display=mobile` +
			`&redirect_uri=${encodeURIComponent(redirectUrlMyBackend)}` +
			`&state=${redirectUrlApp}`,
		})  
		*/  
		let deepLinkToThisApp = getDeepLink()
		await Linking.openURL(
			`https://oauth.vk.com/authorize?response_type=code` +
			`&client_id=${vkontakteAppId}` +
			`&redirect_uri=${encodeURIComponent(redirectUrlMyBackend)}` +
			`&state=${deepLinkToThisApp}` +
			`&display=mobile`
		)
	}
}