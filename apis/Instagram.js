import { AuthSession, Linking } from 'expo'
import { backendUrl, instagramAppId } from './../constants/Apis'
import mainState from './../stores/mobx/mainState'
import { handleAuthRedirect, getDeepLink } from '../helpers/requests'

export default class Instagram {


	async auth () {
		//let redirectUrlApp = AuthSession.getRedirectUrl()
		let redirectUrlMyBackend = backendUrl + 'api/auth/instagram/callback'
		/*
		let result = await AuthSession.startAsync({
			authUrl:
			`https://api.instagram.com/oauth/authorize?response_type=code` +
			`&client_id=${instagramAppId}` +
			`&redirect_uri=${encodeURIComponent(redirectUrlMyBackend)}` +
			`&state=${redirectUrlApp}`,
		})    
		*/
		let deepLinkToThisApp =  getDeepLink()
		await Linking.openURL(
			`https://api.instagram.com/oauth/authorize?response_type=code` +
			`&client_id=${instagramAppId}` +
			`&redirect_uri=${encodeURIComponent(redirectUrlMyBackend)}` +
			`&state=${deepLinkToThisApp}`
		)
	}
}

