import { hostWS, portWS, PUSHER_APP_KEY, PUSHER_APP_CLUSTER } from './../constants/Apis'
import Echo from 'laravel-echo'
import Pusher from 'pusher-js/react-native'

// Enable pusher logging - don't include this in production
//Pusher.logToConsole = true

let pusher = new Pusher(PUSHER_APP_KEY, {
  cluster: 'eu',
  forceTLS: false,
  wsHost: hostWS,
  wsPort: portWS,
})


let ws = new Echo({
  broadcaster: 'pusher',
  key: PUSHER_APP_KEY,
  disableStats: true,
  client: pusher,
})


export default ws