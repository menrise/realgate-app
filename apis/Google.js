import { 
	googleApiKey, 
	googleAppId,
	backendUrl,
	latitudeDiapasonToGetMeetPlacesFromCache,
	longitudeDiapasonToGetMeetPlacesFromCache 
} from '../constants/Apis'
import mainState from './../stores/mobx/mainState'
import { cloneViaJson, makeId } from '../helpers/misc'
import {
	Image,
	ImageStore,
	ImageEditor,
} from 'react-native'
import { AuthSession, Linking } from 'expo'
import { handleAuthRedirect, getDeepLink } from '../helpers/requests'


export default class Google {

	async getMeetPlaces(latitude, longitude, getCache = 1) {
		const query = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='+latitude+','
		+longitude+'&key='+googleApiKey+'&keyword=трц&rankby=distance&language=ru&type=shopping_mall'

		//always use cache (google nearbysearch charge money)
		if(getCache){
			const meetPlacesByCoordsCache = mainState.meetPlacesByCoordsCache.find( (meetPlacesByCoords) => {
				
				//if latitude in cached gap coords
				if(    latitude < meetPlacesByCoords.latitude + latitudeDiapasonToGetMeetPlacesFromCache 
					&& latitude > meetPlacesByCoords.latitude - latitudeDiapasonToGetMeetPlacesFromCache
				){

					//if longitude in cached gap coords
					if(    longitude < meetPlacesByCoords.longitude + longitudeDiapasonToGetMeetPlacesFromCache 
						&& longitude > meetPlacesByCoords.longitude - longitudeDiapasonToGetMeetPlacesFromCache
					){
						return true
					}
				}

				return false
			} )

			
			if(meetPlacesByCoordsCache) {
				console.log('getMeetPlaces() loaded from cache.')
				return mainState.initMeetPlaces( cloneViaJson(meetPlacesByCoordsCache.meetPlaces) )
			}
		}


		return fetch(query).then(
				(response) => response.json()
			).then((responseJson) => {
				console.log('getMeetPlaces() loaded from Google!')
				mainState.initMeetPlaces( cloneViaJson(responseJson.results) )
				const meetPlacesByCoords = { latitude: latitude, longitude: longitude, meetPlaces: cloneViaJson(responseJson.results) }
				mainState.addMeetPlacesByCoordsCache(meetPlacesByCoords)
			}).catch((error) => {
				console.log(error)
			})
	}


	async getPlace(placeId, getCache = true) {

		if(getCache){
			const meetPlace = mainState.meetPlacesCache.find( (meetPlace) => {
				if(meetPlace.placeId === placeId){
					return true
				}
			})

			if(meetPlace){
				console.log('getPlace() loaded from cache.')
				return meetPlace.place
			}
		}

		const query = `https://maps.googleapis.com/maps/api/place/details/json?language=ru&placeid=${placeId}&fields=name,photos,geometry,place_id,id,vicinity&key=${googleApiKey}`
		console.log('getPlace() loaded from Google!')

		return fetch(query).then(
				(response) => response.json()
			).then((responseJson) => {
				if(responseJson.result){
					const meetPlace = {placeId: placeId, place: cloneViaJson(responseJson.result) }
					mainState.addMeetPlacesCache(meetPlace)
				}
				return responseJson.result
			}).catch((error) => {
				console.log(error)
			})
	}


	getPhotoUrl(photoreference, maxwidth = 250, maxheight = 250, getCache = true){

		const imageUrl = `https://maps.googleapis.com/maps/api/place/photo?maxwidth=${maxwidth}&photoreference=${photoreference}&key=${googleApiKey}`

		//always use cache (google images charge money)
		if(getCache){
			const googleImage = mainState.googleImagesCache.find( (image) => {
				if(image.photoreference === photoreference){
					return true
				}
			})

			if(googleImage){
				console.log('getPhotoUrl() image loaded from cache.')
				return googleImage.base64EncodedImage
			}

			//add base64 image to cache
		    Image.getSize(imageUrl, (maxwidth, maxheight) => {
				let imageSettings = {
					offset: { x: 0, y: 0 },
					size: { width: maxwidth, height: maxheight }
				}

				ImageEditor.cropImage(imageUrl, imageSettings, (uri) => {
					 ImageStore.getBase64ForTag(uri, (base64EncodedImage) => {
						
						const googleImage = {photoreference: photoreference, base64EncodedImage: 'data:image/jpeg;base64,' + base64EncodedImage}
						mainState.addGoogleImagesCache(googleImage)
					}, e => console.warn("getBase64ForTag: ", e))
				}, e => console.warn("cropImage: ", e))
			})
		}

	    console.log('getPhotoUrl() image loaded from Google!')
		return imageUrl
	}
	

	async auth () {
		//let redirectUrlApp = AuthSession.getRedirectUrl()
		let redirectUrlMyBackend = backendUrl + 'api/auth/google/callback'
		/*
		let result = await AuthSession.startAsync({
			authUrl:
			`https://accounts.google.com/o/oauth2/v2/auth?response_type=code` +
			`&client_id=${googleAppId}` +
			`&redirect_uri=${encodeURIComponent(redirectUrlMyBackend)}` +
			`&state=${redirectUrlApp}` +
			`&scope=email`,
		})    
		*/

		let deepLinkToThisApp = getDeepLink()
		await Linking.openURL(
			`https://accounts.google.com/o/oauth2/v2/auth?response_type=code` +
			`&client_id=${googleAppId}` +
			`&redirect_uri=${encodeURIComponent(redirectUrlMyBackend)}` +
			`&state=${deepLinkToThisApp}` +
			`&scope=profile email`
		)
	}

}
