import { AuthSession, Linking } from 'expo'
import { backendUrl, fbAppId } from './../constants/Apis'
import mainState from './../stores/mobx/mainState'
import { handleAuthRedirect, getDeepLink } from '../helpers/requests'

export default class Facebook {


	async auth () {
		//let redirectUrlApp = AuthSession.getRedirectUrl()
		let redirectUrlMyBackend = backendUrl + 'api/auth/facebook/callback'
		/*
		let result = await AuthSession.startAsync({
			authUrl:
			`https://www.facebook.com/v2.8/dialog/oauth?response_type=code` +
			`&client_id=${fbAppId}` +
			`&redirect_uri=${encodeURIComponent(redirectUrlMyBackend)}` +
			`&state=${redirectUrlApp}`,
		})    
		*/
		let deepLinkToThisApp = getDeepLink()
		await Linking.openURL(
			`https://www.facebook.com/v2.8/dialog/oauth?response_type=code` +
			`&client_id=${fbAppId}` +
			`&redirect_uri=${encodeURIComponent(redirectUrlMyBackend)}` +
			`&state=${deepLinkToThisApp}`
		)
	}
}