import React from 'react'
import {
  ActivityIndicator,
  AsyncStorage,
  Button,
  StatusBar,
  StyleSheet,
  View,
  Platform,
} from 'react-native'
import { createStackNavigator, createSwitchNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation'

import SignInScreen from '../screens/auth/SignInScreen'
import OrderMeetScreen from '../screens/OrderMeet/OrderMeetScreen'
import MyMeetsListScreen from '../screens/MyMeets/MyMeetsListScreen'
import MeetTimeScreen from '../screens/MeetTime/MeetTimeScreen'
import SettingsScreen from '../screens/SettingsScreen'
import TabBarIcon from '../components/TabBarIcon'


const AuthStack = createStackNavigator({ SignIn: SignInScreen })

const OrderMeetStack = createStackNavigator({ OrderMeet: OrderMeetScreen})
OrderMeetStack.navigationOptions = {
  tabBarLabel: 'Участвовать',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-walk${focused ? '' : '-outline'}` : 'md-walk'}
    />
  ),
}

const MyMeetsListStack = createStackNavigator( {MyMeetsList: MyMeetsListScreen} )
MyMeetsListStack.navigationOptions = {
  tabBarLabel: 'Мои встречи',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-list${focused ? '' : '-outline'}` : 'md-list'}
    />
  ),
}

const MeetTimeStack = createStackNavigator( {MeetTime: MeetTimeScreen} )
MeetTimeStack.navigationOptions = {
  tabBarLabel: 'Текущая встреча',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-people${focused ? '' : '-outline'}` : 'md-people'}
    />
  ),
}

const SettingsStack = createStackNavigator( {Settings: SettingsScreen} )
SettingsStack.navigationOptions = {
  tabBarLabel: 'Настройки',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-options${focused ? '' : '-outline'}` : 'md-options'}
    />
  ),
}


const appScreens = createBottomTabNavigator({
  OrderMeetStack,
  MyMeetsListStack,
  MeetTimeStack,
  SettingsStack,
})

const switchNavigator = createSwitchNavigator(
  {
    App: appScreens,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'Auth',
  }
)

const appContainer = createAppContainer(switchNavigator)

export default appContainer