//googleAppId is not same app for what issued googleApiKey
export const googleAppId = '979045338320-6877r2tm16tmdnhs3q08p8t0c3luh3h7.apps.googleusercontent.com'
//TODO SUBMIT INST APP FOR DISABLE INST APP SANDBOX
export const instagramAppId = '82cc4eed4a5c427eb60a5a2c3ae74b04'

export const googleApiKey = 'AIzaSyB7NMBsN29C9cu1ybNNmKI1uc7KGjE9ZHE'

export const fbAppId = '735233916813897'
export const vkontakteAppId = '6809940'

//const backendDomain = '192.168.1.8' //local server 
const backendDomain = 'realgateapp.ru' //production server

//axios get Network error with https with ip. Because we using http on local server
//export const backendUrl = 'http://' + backendDomain + '/' //local server  
export const backendUrl = 'https://' + backendDomain + '/' //production server

export const backendUrlHttp = 'http://' + backendDomain + '/'
export const hostWS = backendDomain
export const portWS = '6001'
export const PUSHER_APP_KEY = 'c615c58c8d5bbb6e09a2'
export const PUSHER_APP_CLUSTER = 'eu'


//diapason it is gap from cached latitude/longitude within will be return same meet places 
export const latitudeDiapasonToGetMeetPlacesFromCache = 0.07
export const longitudeDiapasonToGetMeetPlacesFromCache = 0.07

//user coordinate at server uses for send push near users about meets
//this diapason triger update user coords at server
export const userLatitudeDiapasonTrigerToSendNewToServer = 0.07
export const userLongitudeDiapasonTrigerToSendNewToServer = 0.07