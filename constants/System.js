export const meetRoundPeriod = 60 * 5 // seconds
export const gapBetweenLastApproveAndRealMeetStart = 0 // seconds
export const minutesPeriodBeforeMeetUserCanSeeMembersCount = 60 * 6 // in period 6 hours before meet and after meet we display members count

export const stepForMeetGroup1vs1 = 9 //step user_group from multi pairs to 1vs1