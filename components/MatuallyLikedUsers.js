
import { View, Text, StyleSheet, Platform, Alert } from 'react-native'
import React from 'react'
import { observer, inject } from 'mobx-react/native'
import UserBadge from './UserBadge'
import { makeId } from './../helpers/misc'
import { Icon } from 'react-native-elements'


export default class MatuallyLikedUsers extends React.Component {


  getMutuallyLikedUsers(likes, user_id){

    const mutuallyLikedUsers = likes.map( (user1) => {
      if(user1.pivot.liker_user_id === user_id){

        const isMutually = likes.some( (user2) => {
          if(user2.pivot.liker_user_id === user1.id && user2.pivot.liked_user_id === user_id) return true
        })

        if(isMutually) {
          //user1.sex - current user sex in setting (it may differ what user sex registered to meet)
          //in MeetingProcessor display sex what user had when registered to meet
          user1.pivot.sex = user1.sex //UserBadge get sex from pivot
          return user1
        }
      }
    })

    const mutuallyLikedUsersCleared = mutuallyLikedUsers.filter(element => element !== undefined)

    return mutuallyLikedUsersCleared
  }

  render() {
    const { likes, user_id } = this.props

    return (
      <View>
        {
          (    
            ( mutuallyLikedUsers = this.getMutuallyLikedUsers(likes, user_id) )
            && mutuallyLikedUsers.length > 0
          )
            ? (
              <View>
                <Text style={styles.simpletext}>У вас взаимные симпатии с: </Text>
                {mutuallyLikedUsers.map( (mutuallyLikedUser) => 
                  <View key={makeId()}>
                    <UserBadge user={mutuallyLikedUser} phoneCallOnPress={true} />
                  </View>
                )}
              </View>
            )
            : <View>
                <Text style={styles.simpletext}>- в этот раз, у вас нет взаимных симпатий</Text>
                <Icon
                  name={Platform.OS === 'ios' ? `question-circle-o` : 'question-circle-o'}
                  type='font-awesome'
                  color='#bbb'
                  size={18}
                  containerStyle={styles.helpIcon}
                  onPress={ () => Alert.alert('Почему нет взаимных симпатий?',
                    `Не расстраивайтесь это совершенно нормально, люди не часто сходятся в симпатиях, но найти человека с которым вы нравитесь друг другу прекрасно.

Сделать это можно только знакомясь с новыми людьми. Мы в свою очередь максимально упростим этот процесс для вас. Пробуйте и вы точно встретите свою половинку!`
                  )}
                />
              </View>
        }
      </View>
    )
  }
}


const styles = StyleSheet.create({
  simpletext: {
    margin: 10,
  },
  helpIcon:{
    marginBottom: 8,
    opacity: 0.8,
  }
})