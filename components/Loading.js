import React from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native'
import Colors from './../constants/Colors'

export default class Loading extends React.Component {
  render() {
    return (
    	<View style={{padding:20}}>
			  <ActivityIndicator size="large" color={Colors.tintColor} />
      </View>
    )
  }
}


const styles = StyleSheet.create({
  simpletext: {
    margin: 10,
  }
})