import { Text, View, StyleSheet, Linking } from 'react-native'
import React from 'react'

export default class Anchor extends React.Component {
  _handlePress = () => {
    Linking.openURL(this.props.href)
    this.props.onPress && this.props.onPress()
  }

  render() {
    return (
      <Text {...this.props} onPress={this._handlePress} style={[styles.link, this.props.style]}>
        {this.props.children}
      </Text>
    )
  }
}

const styles = StyleSheet.create({
  link: {
    textDecorationStyle: 'solid',
    textDecorationLine: 'underline',
  }
})

// <Anchor href="https://google.com">Go to Google</Anchor>
// <Anchor href="mailto://support@expo.io">Email support</Anchor>