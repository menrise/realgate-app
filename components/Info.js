import React from 'react'
import { Text, View, StyleSheet } from 'react-native'

export default class Info extends React.Component {
  render() {
    return (
  	  <View>
  	    <Text style={[styles.paragraph, this.props.style]} onPress={this.props.onPress}>
  	      {this.props.text}
  	    </Text>
  	  </View>
    )
  }
}


const styles = StyleSheet.create({
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
})