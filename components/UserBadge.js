import React from 'react'
import { Text, View, StyleSheet, Platform, TouchableHighlight, Alert } from 'react-native'
import { Icon, badge } from 'react-native-elements'
import { observer, inject } from 'mobx-react/native'
import Communications from 'react-native-communications'


@inject('mainState')
@observer
export default class Info extends React.Component {
  render() {
    const { user, pair, mainState, phoneCallOnPress } = this.props

    if(user){
        return (
            <View style={styles.badgeIconView}>

                {
                (user.pivot.sex === 2)
                    ? <Icon containerStyle={styles.badge}
                        name={Platform.OS === 'ios' ? `ios-woman` : 'md-woman'}
                        type='ionicon'
                        color='#bbb'
                    /> 
                    : <Icon containerStyle={styles.badge}
                        name={Platform.OS === 'ios' ? `ios-man` : 'md-man'}
                        type='ionicon'
                        color='#bbb'
                    />
                }
                <Text style={ (pair == true) ? styles.textStrong : styles.text } >
                    { user.name.substr(0, 16) } - id:{user.id}
                </Text>

                <Text style={ (pair == true) ? styles.textStrong : styles.text } >
                    { 
                        (pair == true) 
                            ? 'ваша пара'
                            : (mainState.user_id === user.id)
                                ? 'это вы'
                                : '      '
                    }
                </Text>

                {
                (phoneCallOnPress)
                    ? <View style={styles.phoneWrapper}>
                        <TouchableHighlight
                            onPress={() => {
                                if(user.phone_number){
                                    Communications.phonecall(user.phone_number, true)
                                }else{
                                    Alert.alert('Нет номера', 'К сожалению, этот пользователь не указал свой телефон')
                                }
                            }}
                        >
                            <Icon containerStyle={styles.phoneIcon}
                                name={Platform.OS === 'ios' ? `phone` : 'phone'}
                                type='entypo'
                                color='#bbb'
                            />
                        </TouchableHighlight>
                    </View>
                    : null
                }
            </View>
        )
    }else{
        return (
            <View style={styles.badgeIconView}>
                <Text style={styles.text} >
                    нет пары
                </Text>
            </View>
        )
    }


  }
}


const styles = StyleSheet.create({
    badgeIconView:{
        padding: 5,   
        paddingLeft: 10,  
        paddingRight: 10,       
        backgroundColor:'#ddd',
        borderRadius: 5,
        margin: 3,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    badge:{
        paddingRight: 10,
    },
    text: {
        //
    },
    textStrong:{
        fontWeight: 'bold',
    },
    phoneWrapper: {
        //
    }
})