import React from 'react';
import { ScrollView, StyleSheet, Text, View, Button, Platform, Alert } from 'react-native'
import { observer, inject } from 'mobx-react/native'
import { makeId, calcApproveDecides, canMeetStart, getUserById } from './../../helpers/misc'
import * as Animatable from 'react-native-animatable'
import WaitMembersList from './WaitMembersList'
import MemberConfirm from './MemberConfirm'
import MeetingProcesor from './MeetingProcesor'
import { ConfirmDialog } from 'react-native-simple-dialogs'
import { List, ListItem, Icon } from 'react-native-elements'
import Info from './../../components/Info'
import Loading from './../../components/Loading'
import { meetRoundPeriod } from './../../constants/System'


@inject('mainState')
@observer
export default class MeetTimeScreen extends React.Component {

  static navigationOptions = {
    title: 'Текущая встреча',
  }

  meetUpdateSubscribed = false
  meetId = null
  canMeetStart = false

  constructor(props) {
    super(props)
    this.state = {
      decidedMembering: false,
    }
  }

  decideMembering(){
    this.setState( {decidedMembering: true} )
  }

  subscribeToMeet(meetId){
    if(!this.meetUpdateSubscribed){
      const { mainState } = this.props

      this.meetUpdateSubscribed = true
      this.meetId = meetId

      mainState.ws.channel('meet.' + this.meetId)
        .listen('MeetUpdate', (event) => {
            mainState.getMeets(true) //TODO get only one meet by id
        })
    }
  }

  componentWillUnmount(){
    if(this.meetUpdateSubscribed){
      const { mainState } = this.props
      mainState.ws.leave('meet.' + this.meetId)
    }
  }



  render() {

    const { mainState } = this.props
    const waitedMeets = mainState.userMeetsInPorgressInWaitPeriod
    const meetsInPorgress = mainState.userMeetsInPorgress
    const roundTimeInMins = parseInt(meetRoundPeriod / 60)

    ////info screen (not meet in progress)
    if(!meetsInPorgress.length){
      return <View>

        <Info
          text={`Сейчас не происходит встреча.`} 
        />

        <Info
          text={`Во время встречи вы увидите здесь интерактивный экран, вам будет нужно держать его открытым и следовать его инструкциям.`} 
        />

        <Info
          text={`Как проходит встреча?`} 
          onPress={ () => Alert.alert('События во время встречи',
            `Встреча автоматизирована и в роли ведущего выступает это приложение.

В начале встречи вам необходимо подтвердить что вы на месте и готовы начать. После этого вы увидите список участников с их статусами готовности.

Знакомства начнутся после подтверждения готовности всех, либо через 15 минут ожидания опоздавших.

Встреча будет состоять из нескольких знакомств один на один по `+roundTimeInMins+` минут каждое.

При каждом знакомстве вы будете видеть имя своей текущей пары (приложение будет распределять пары, показывать оставшееся время каждого знакомства и оповещать звоном колокольчика о его окончании).

До начала общения один на один познакомьтесь с участниками чтобы знать их по именам, если вы забудете кого-то, не стесняйтесь спросить имя.

Понять что перед вами участник вы сможете по телефону в руке с этим приложением. Сами тоже не забывайте держать телефон в руке с открытым приложением.

Если вы не уверены что перед вами участник, спросите - "Здравствуйте, вы пришли на встречу RealGate (рилгэйт)?"

После того как вы увидите в приложении имя своей пары, вам нужно будет предложить начать общение (если пара не сделает это раньше вас) и отойти на несколько шагов от других участников (чтобы беседа была один на один). После этого вы будете общаться до конца текущего этапа. Затем происходит смена пары.

После всех знакомств вы сможете выбрать в приложении понравившихся людей и при взаимной симпатии увидите контакты друг друга.
`
          )}
          style={{textDecorationLine: 'underline'}}
        />
      </View>
    }



    const authUserInCurrentMeet = getUserById(meetsInPorgress[0].users, mainState.user_id)      
    //unapproved meet screen
    if(authUserInCurrentMeet && authUserInCurrentMeet.pivot.approve_membering === 0){ 
      return <Info text='Вы сообщили о том что не сможете присутствовать в текущей встрече. Мы будем рады видеть вас в новых встречах.' />
    }


    ////members ready list screen
    if(waitedMeets.length && authUserInCurrentMeet && authUserInCurrentMeet.pivot.approve_membering === 1){
      this.subscribeToMeet(waitedMeets[0].id)
      return <WaitMembersList waitedMeet={waitedMeets[0]} />
    }

    if(authUserInCurrentMeet && authUserInCurrentMeet.pivot.approve_membering === null && meetsInPorgress.length && !waitedMeets.length){ 
      return <Info text='К сожалению, вы не успели подтвердить своё участие. Встреча идёт без вас. Мы будем рады видеть вас в новых встречах.' />
    }


    const decides = calcApproveDecides(meetsInPorgress[0])
    this.canMeetStart = canMeetStart(decides, meetsInPorgress, waitedMeets)


    ////meet is going screen
    if(this.canMeetStart) return <MeetingProcesor meet={meetsInPorgress[0]} />


    //meet is canceled screen (see only who approved membering)
    if(!this.canMeetStart && meetsInPorgress.length && !waitedMeets.length){
      return <Info text='К сожалению встреча не состоится. Другие участники не смогли присутствовать. Мы будем рады видеть вас в новых встречах.' />
    }


    ////meet time question screen
    if( !this.state.decidedMembering && waitedMeets.length ) return <MemberConfirm decideMembering={this.decideMembering.bind(this)} />


    ////temporary screen
    return <Loading />

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  solid: {
    fontWeight: 'bold',
  },
  helpIcon:{

    opacity: 0.8,
  }
})
