import React from 'react';
import { ScrollView, StyleSheet, Text, View, Button, Platform } from 'react-native'
import { observer, inject } from 'mobx-react/native'
import { makeId, calcApproveDecides, canMeetStart, getUserById } from './../../helpers/misc'
import * as Animatable from 'react-native-animatable'
import { ConfirmDialog } from 'react-native-simple-dialogs'


@inject('mainState')
@observer
export default class MemberConfirm extends React.Component {

  static navigationOptions = {
    title: 'Текущая встреча',
  }
  

  constructor(props) {
    super(props)
    this.state = {
      waitMePressed: false,
      approveMemberingPressed: false,
      unapproveMemberingPressed: false, 
      confirmApproveMemberingVisible: false,
      confirmUnapproveMemberingVisible: false,
    }
  }


  render() {

    const { mainState, decideMembering } = this.props
    const waitedMeets = mainState.userMeetsInPorgressInWaitPeriod

    ////meet time question screen
    return (
      <View style={styles.container}>
        <Button style={styles.button} title='Я на месте и готов начать'
          onPress={ () => this.setState( {approveMemberingPressed: true} ) || this.setState( {confirmApproveMemberingVisible: true} ) }
          disabled={this.state.approveMemberingPressed} />
        <Button style={styles.button} 
          title={ (this.state.waitMePressed)
            ? 'Вас будут ждать ещё ' + waitedMeets[0].timeBeforeEndWaitPeriod 
              + '. До конца этого периода вам нужно будет успеть подтвердить готовность'
            : 'Я прошу подождать меня (вас будут ждать ещё ' + waitedMeets[0].timeBeforeEndWaitPeriod + ')'
          }
          onPress={() => this.setState( {waitMePressed: true} )}
          disabled={this.state.waitMePressed} />
        <Button style={styles.button} title='Я не смогу присутствовать или не успею в период ожидания'
          onPress={ () => this.setState( {unapproveMemberingPressed: true} ) || this.setState( {confirmUnapproveMemberingVisible: true} ) }
          disabled={this.state.unapproveMemberingPressed} />


        <ConfirmDialog
          title="Подтверждение участия"
          message="Вы готовы начать?"
          visible={this.state.confirmApproveMemberingVisible}
          onTouchOutside={ () => this.setState( {approveMemberingPressed: false} ) || this.setState({confirmApproveMemberingVisible: false}) }
          positiveButton={{
              title: "Да",
              onPress: async () => {
                await mainState.approveMembering(waitedMeets[0].id)
                this.setState( {confirmApproveMemberingVisible: false} )
                decideMembering()
              }
          }}
          negativeButton={{
              title: "Нет",
              onPress: () => this.setState( {approveMemberingPressed: false} ) || this.setState( {confirmApproveMemberingVisible: false} )
          }}
        />

        <ConfirmDialog
          title="Отмена участия"
          message="Вы уверены что хотите отменить участие?"
          visible={this.state.confirmUnapproveMemberingVisible}
          onTouchOutside={ () => this.setState( {unapproveMemberingPressed: false} ) || this.setState( {confirmUnapproveMemberingVisible: false}) }
          positiveButton={{
              title: "Да",
              onPress: async () => {
                await mainState.unapproveMembering(waitedMeets[0].id)
                this.setState( {confirmUnapproveMemberingVisible: false} )
                decideMembering()
              }
          }}
          negativeButton={{
              title: "Нет",
              onPress: () => this.setState( {unapproveMemberingPressed: false} ) || this.setState( {confirmUnapproveMemberingVisible: false} )
          }}
        />
      </View>
    )

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
})
