import React from 'react';
import { ScrollView, StyleSheet, Text, View, Button, Platform, Alert } from 'react-native'
import { observer, inject } from 'mobx-react/native'
import { makeId, getUserById, calcApproveDecides, cloneViaJson, generateMeetRounds, getUser } from './../../helpers/misc'
import * as Animatable from 'react-native-animatable'
import { List, ListItem, Icon, CheckBox } from 'react-native-elements'
import Info from './../../components/Info'
import Loading from './../../components/Loading'
import UserBadge from './../../components/UserBadge'
import { showMessage } from 'react-native-flash-message'


@inject('mainState')
@observer
export default class LikeTime extends React.Component {
  
  static navigationOptions = {
    title: 'Текущая встреча',
  }

  constructor(props) {
    super(props)
    this.state = {
      likes: {},
      likeConfirmed: false,
      likeConfirmPresed: false,
    }
  }

  getUsersForLikes(decides, user){

    let usersForLikes = []

    if(decides.isMultisex){
      return (user.pivot.sex === 2) ? decides.mansApprove : decides.womansApprove
    }else{
      if(decides.isMonosexMan){
        return decides.mansApprove.filter( (man) => man.id !== user.id )
      }else{
        return decides.womansApprove.filter( (woman) => woman.id !== user.id )
      }
    }
  }

  async confirmLike(meetId){
    this.setState( {likeConfirmPresed: true} )

    const { mainState } = this.props
    const likes = this.state.likes

    let liked_user_ids_comma_separated = ''
    let separator = ''
    
    for (prop in likes) {
      if(likes[prop]){
        liked_user_ids_comma_separated += separator + prop
        separator = ','
      }
    }

    if(liked_user_ids_comma_separated){

      const response = await mainState.api.like(meetId, liked_user_ids_comma_separated)

      if(response !== undefined && response.status === 200){
        showMessage({
          message: "Сохранено",
          type: "success",
        })

        this.setState( {likeConfirmed: true} )
      }
      
    }else{
      this.setState( {likeConfirmed: true} )
    }

    this.setState( {likeConfirmPresed: false} )
  }


  render() {
    const { mainState, roundInfo, decides, meet} = this.props

    const user = getUser(meet.users, mainState.user_id)
    const usersForLikes = this.getUsersForLikes(decides, user)

    if(this.state.likeConfirmed) {
      return (
        <View style={styles.container}>
          <View style={styles.paragraphContainer}>
            <View style={styles.hintButtonWarper}>
                <Info text={`Вы закончили этап лайков.

Через ${roundInfo.timeBeforeRoundEnd} вы сможете увидеть взаимные симпатии.

Будем ждать вас в новых встречах ;)`} />
            </View>
          </View>
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <View style={styles.paragraphContainer}>
          <View style={styles.hintButtonWarper}>
              <Info text='Выберите понравившихся участников.' />
              <Icon
                name={Platform.OS === 'ios' ? `question-circle-o` : 'question-circle-o'}
                type='font-awesome'
                color='#bbb'
                onPress={ () => Alert.alert('Взаимная симпатия', `При взаимной симпатии вы сможете видеть телефон понравившегося человека на экране прошедшей встречи.

Если вы не указали свой телефон то понравившийся вам человек не сможет с вами связаться.`) }
              />
          </View>
          <Text style={styles.paragraphTime}>
            До конца выбора - {roundInfo.timeBeforeRoundEnd}. 
          </Text>
        </View>

        <ScrollView contentContainerStyle={styles.scrollContainer}>
          {
            usersForLikes.map( (user) => {
              return <CheckBox
                center
                title={ 'Id:' + user.id + ' - ' + user.name.substr(0, 16) }
                key={user.id}
                containerStyle={styles.likeCheckBox}
                checked={this.state.likes[user.id]}
                onPress={() => {
                  this.state.likes[user.id] = !this.state.likes[user.id]
                  this.setState( {likes: this.state.likes} )
                }}
              />
              
            })
          }
          <View style={{marginTop:25}} />
          <Button title="Подтвердить"
            disabled={this.state.likeConfirmPresed}
            onPress={ async () => await this.confirmLike(meet.id) } />
        </ScrollView>
      </View>
    )

  }
}


const width = 320
const borderRadius = 4
const styles = StyleSheet.create({

  container: {
    alignItems: 'center',
    justifyContent:'center',
  },
  scrollContainer: {
    paddingVertical: 10,
    paddingBottom: 50,
  },
  paragraphContainer: {
    margin: 15,
    backgroundColor: '#fff',
    borderRadius: borderRadius,
    padding: 10,
    width:width,
  },
  hintButtonWarper: {
    margin: 0,
  },
  paragraphTime: {
    margin: 3,
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  likeCheckBox: {
    borderRadius: borderRadius,
    width:width,
  },
})
