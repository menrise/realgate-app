import React from 'react';
import { StyleSheet, Text, View, Button, Platform } from 'react-native'
import { observer, inject } from 'mobx-react/native'
import { List, ListItem, Icon } from 'react-native-elements'
import Info from './../../components/Info'


@inject('mainState')
@observer
export default class WaitMembersList extends React.Component {

  static navigationOptions = {
    title: 'Текущая встреча',
  }


  render() {

    const { waitedMeet } = this.props

    return (
      <View style={styles.container} >
        <Text style={styles.paragraph}>Ожидание участников - {waitedMeet.timeBeforeEndWaitPeriod}</Text>

        <List
          containerStyle={{marginTop: 0, width: 300}}>
          {
            waitedMeet.users.map( (user, index) => {
              return <ListItem
                key={ user.id }
                title={ '   ' + (index + 1) + ' - ' +  user.name }
                leftIcon={ (user.pivot.sex === 2)
                  ? <Icon
                    name={Platform.OS === 'ios' ? `ios-woman` : 'md-woman'}
                    type='ionicon'
                    color='#bbb'
                  /> 
                  : <Icon
                    name={Platform.OS === 'ios' ? `ios-man` : 'md-man'}
                    type='ionicon'
                    color='#bbb'
                  />
                }
                rightIcon={ (user.pivot.approve_membering)
                  ? <Icon
                    name='check'
                    type='evilicon'
                    color='green'
                  />
                  : <Icon
                    name='question'
                    type='evilicon'
                    color='#666'
                  />
                }
              />
            })
          }
        </List>
      </View>
    )
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
})
