import React from 'react';
import { ScrollView, StyleSheet, Text, View, Button, Platform, Alert } from 'react-native'
import { observer, inject } from 'mobx-react/native'
import { makeId, getUserById, calcApproveDecides, cloneViaJson, generateMeetRounds, getUser } from './../../helpers/misc'
import * as Animatable from 'react-native-animatable'
import { List, ListItem, Icon } from 'react-native-elements'
import Info from './../../components/Info'
import Loading from './../../components/Loading'
import UserBadge from './../../components/UserBadge'
import LikeTime from './LikeTime'
import { timeFormatDB, nowMoment, periodBetween, getRoundInfo, getRealTimeStart } from './../../helpers/time'
import { meetRoundPeriod } from './../../constants/System'
import MatuallyLikedUsers from './../../components/MatuallyLikedUsers'
import { playSound } from './../../helpers/sounds'
import { showMessage } from 'react-native-flash-message'


@inject('mainState')
@observer
export default class MeetTimeScreen extends React.Component {

  meetRealStartTimeInterval

  vars
  
  static navigationOptions = {
    title: 'Текущая встреча',
  }


  constructor(props) {
    super(props)
    this.state = { 
      secondsAfterRealStart: null,
      isMeetPlanDisplayed: false,
    }

    this.vars = {}
  }

  componentWillMount(){
    const { mainState, meet } = this.props

    const realStartTime = getRealTimeStart(meet.user_device_time, meet.serverTime.date, meet.users)

    const meetsRealStartTime = mainState.setMeetsRealStartTime( meet.id, realStartTime )

    this.meetRealStartTimeInterval = setInterval( () => {
      const secondsAfterRealStart = periodBetween(nowMoment(), meetsRealStartTime.realStartTime, null, true)
      this.setState( {secondsAfterRealStart: secondsAfterRealStart} )
    }, 1000)
  }

  componentWillUnmount(){
    clearInterval(this.meetRealStartTimeInterval)
  }

  componentWillUpdate(nextProps, nextState){
    const { mainState, meet } = nextProps
    const decides = calcApproveDecides(meet)
    const rounds = generateMeetRounds(decides)
    const roundInfo = getRoundInfo(this.state.secondsAfterRealStart, meetRoundPeriod)
    const pairOfUserInCurrentRound = this.getPairOfUser(rounds[roundInfo.roundIndex], mainState.user_id)
    const isLikeTime = (rounds.length + 1 === roundInfo.round) ? true : false
    const isMeetEnded = (rounds.length < roundInfo.round && !isLikeTime) ? true : false


    this.vars.decides = decides
    this.vars.rounds = rounds
    this.vars.roundInfo = roundInfo
    this.vars.pairOfUserInCurrentRound = pairOfUserInCurrentRound
    this.vars.isLikeTime = isLikeTime
    this.vars.isMeetEnded = isMeetEnded


    if(isMeetEnded && !this.vars.gotMeetsAfterLikeTime){
      this.vars.gotMeetsAfterLikeTime = true
      mainState.getMeets(true)    
    }

    //round change (with like time and end screen)
    if(roundInfo.round && this.vars.lastRound !== roundInfo.round && roundInfo.round <= rounds.length + 2){
      this.vars.lastRound = roundInfo.round
      
      playSound('roundChange')

      showMessage({
        message: "Смена этапа",
        type: "success",
        duration: 3000
      })
    }
  }

  getPairOfUser(pairs, userId){
    if(!pairs) return null

    let pairOfUser = null
    pairs.some( (pair) => {
      if(pair.member1.id === userId){
        pairOfUser = pair.member2
        return true
      }else if(pair.member2.id === userId){
        pairOfUser = pair.member1
        return true
      }
    })

    return pairOfUser
  }


  render() {


    if(Object.keys(this.vars).length === 0){
      return <Loading />
    }

    const { mainState, meet } = this.props
    const { decides, rounds, roundInfo, pairOfUserInCurrentRound, isLikeTime, isMeetEnded } = this.vars


    if(isLikeTime){
      return <LikeTime roundInfo={roundInfo} meet={meet} decides={decides}/>
    }

    if(isMeetEnded){
      return <View style={styles.container}>
        <Info text='Встреча закончилась. Будем рады видеть вас в новых встречах.' />
        <MatuallyLikedUsers likes={meet.likes} user_id={mainState.user_id} />
      </View>
    }

    return (
      <View style={styles.container}>
        <View style={styles.paragraphContainer}>
          <View style={styles.hintButtonWarper}>
            <Button title="Что мне делать?" onPress={ () => Alert.alert('Ваши действия',
             `Найдите свою пару среди участников, предложите отойти на несколько шагов от других пар и общайтесь в течении этапа.

Это будет повторяться с новым человеком в последующих этапах (если участников больше 2х).

В процессе общения расскажите немного о себе и спросите интересующее вас ;)`) }
            />
          </View>
          <Text style={styles.paragraphTime}>
            Текущий этап - {roundInfo.round}. До конца этапа - {roundInfo.timeBeforeRoundEnd}. 
          </Text>
          { (pairOfUserInCurrentRound) 
            ? <UserBadge user={pairOfUserInCurrentRound} pair={true} />
            : <Text style={styles.paragraphTime}>У вас нет пары в этом этапе, подождите следующий.</Text>  }
        </View>
        <ScrollView contentContainerStyle={styles.scrollContainer}>
          <View style={{paddingLeft: 5, paddingRight: 5}}>
            <Button title={ 'План знакомств ' + ( (this.state.isMeetPlanDisplayed) ? '(скрыть)' : '(показать)' )}
              style={{fontWeight: 'bold', marginLeft: 5, marginRight: 5}}
              onPress={ () => this.setState( {isMeetPlanDisplayed: !this.state.isMeetPlanDisplayed} ) }
            />
          </View>
          <Text >
            &nbsp;
          </Text>
          {
            this.state.isMeetPlanDisplayed && rounds.map( (round, roundIndex) => {
              return <View style={ (roundIndex === roundInfo.roundIndex) ? styles.currentRound : styles.round } key={ makeId() }>
                <Text>Этап {roundIndex + 1} { (roundIndex === roundInfo.roundIndex) ? ' - текущий' : null }</Text>
                {
                  round.map( (pair, pairIndex) => {
                    return(
                      <View style={ (roundIndex === roundInfo.roundIndex) ? styles.currentPair : styles.pair } key={ makeId() } >
                        <Text>Пара {pairIndex + 1}</Text>
                        <UserBadge style={styles.userBadge} user={pair.member1} />
                        <UserBadge style={styles.userBadge} user={pair.member2} />
                      </View>
                    )
                  })
                }
              </View>
            })
          }
        </ScrollView>
      </View>
    )

  }
}

const styles = StyleSheet.create({
  scrollContainer: {
    paddingVertical: 20,
    paddingBottom: 50,
  },
  container: {
    alignItems: 'center',
    justifyContent:'center'
  },
  paragraphContainer: {
    margin: 3,
    backgroundColor: '#fff',
    borderRadius: 4,
    padding: 10,
    width:320,
  },
  hintButtonWarper: {
    margin: 7,
  },
  paragraph: {
    margin: 3,
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  paragraphTime: {
    margin: 3,
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  pair: {
    marginBottom: 10,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#d6d7da',
    width: 320,
    padding: 10,

  },
  currentPair: {
    marginBottom: 10,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'green',
    width:320,
    padding: 10,

  },
  userBadge: {
    flex: 3,
  },
  round: {
    marginBottom: 30,
  },
  currentRound: {
    marginBottom: 30,
  },
})
