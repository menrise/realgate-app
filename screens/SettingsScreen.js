import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  Button,
  AsyncStorage,
  Picker,
  Platform,
  Alert,
  TextInput,
} from 'react-native';
import DatePicker from 'react-native-datepicker'
import moment from 'moment-timezone-all'
import { observer, inject } from 'mobx-react/native'
import { action } from 'mobx'
import { showMessage } from 'react-native-flash-message'
import PhoneInput from 'react-native-phone-input'
import { Icon } from 'react-native-elements'


@inject('mainState')
@observer
export default class HomeScreen extends React.Component {


  phone

  constructor (){
    super()
    this.state = { 
      userName: null,
    }
  }

  static navigationOptions = {
    title: 'Настройки',
  }
  

  render() {
    const { mainState } = this.props

    return (
      <View style={styles.container}>

        <View >
          <View style={styles.pickerContainer}>
            <Text style={styles.pickerTitle}>Ваш пол:</Text>
            <Picker 
              selectedValue={mainState.sex}
              style={ styles.picker }
              onValueChange={ (itemValue, itemIndex) => mainState.set( {name: 'sex', value: itemValue} )  }>
              <Picker.Item style={ styles.pickerItem } label="не выбран" value="n" />
              <Picker.Item style={ styles.pickerItem } label="мужской" value={ 1 } />
              <Picker.Item style={ styles.pickerItem } label="женский" value={ 2 } />
            </Picker>
          </View>

          <View style={styles.pickerContainer}>
            <Text style={styles.pickerTitle}>Дата рождения:</Text>
            <DatePicker
              style={{width: 200}}
              date={mainState.birth_date}
              mode="date"
              placeholder="Выберите дату"
              format="YYYY-MM-DD"
              minDate={mainState.min_birth_date}
              maxDate={mainState.max_birth_date}
              confirmBtnText="Ок"
              cancelBtnText="Отмена"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  right: 0,
                  top: 4,
                  
                },
                dateInput: {
                  marginLeft: 30
                },
              }}
              onDateChange={ (birth_date) => mainState.set( {name: 'birth_date', value: birth_date} ) }
            />
          </View>         

          <View style={styles.pickerContainer}>
            <Text style={styles.pickerTitle}>Знакомитесь с:</Text>
            <Picker 
              selectedValue={mainState.sex_need}
              style={ styles.picker2 }
              onValueChange={(itemValue, itemIndex) => mainState.set( {name: 'sex_need', value: itemValue} ) }>
              <Picker.Item style={ styles.pickerItem } label="не выбрано" value="n" />
              <Picker.Item style={ styles.pickerItem } label="мужчинами" value={ 1 } />
              <Picker.Item style={ styles.pickerItem } label="женщинами" value={ 2 } />
            </Picker>
          </View>

          <View style={styles.pickerContainer}>
            <Text style={styles.pickerTitle}>Телефон: </Text>
            <Icon containerStyle={styles.badge}
              name={Platform.OS === 'ios' ? `question-circle-o` : 'question-circle-o'}
              type='font-awesome'
              color='#bbb'
              onPress={ () => Alert.alert('Номер телефона', 'Используется для обмена контактами при взаимной симпатии') }
            />
            <PhoneInput
              ref={ref => {
                this.phone = ref
              }}
              initialCountry='ru'
              cancelText='Отмена'
              confirmText='Подтвердить'
              style={{
                height: 50,
                width: 170,
                marginLeft: 30,
              }}
              value={mainState.phone_number}
            />
          </View>

          <View style={styles.pickerContainer}>
            <Text style={styles.pickerTitle}>Ваше имя: </Text>
            <Icon containerStyle={styles.badge}
              name={Platform.OS === 'ios' ? `question-circle-o` : 'question-circle-o'}
              type='font-awesome'
              color='#bbb'
              onPress={ () => Alert.alert('Ваше имя', 'Показывается другим участникам во время встречи') }
            />
            <TextInput
              style={{borderColor: '#aaa', marginLeft: 30, padding: 5, paddingLeft: 10, paddingRight: 10}}
              onChangeText={(user_name) => {
                user_name = user_name.replace(/([^A-Za-zА-Яа-яёЁ]*)/, '')
                mainState.set( {name: 'user_name', value: user_name} )
              }}
              value={mainState.user_name}
              maxLength={17}
              editable={true}
              spellCheck={false}
              defaultValue={mainState.user_name}
            />
          </View>

          <View style={styles.buttonContainer}>
            <Button  title="Сохранить настройки"
              disabled={mainState.saveSettingsInProcess}
              onPress={this._saveSettings} />
          </View>

        </View>

        <Button title="Выйти из аккаунта" onPress={this._signOutAsync} />

      </View>
    );
  }

  _saveSettings = async () => {
    const { mainState } = this.props
    const phoneNumberValue = this.phone.getValue()
    const isValidPhoneNumber = this.phone.isValidNumber()
    const sex = mainState.sex
    const sex_need = mainState.sex_need
    const birth_date = mainState.birth_date
    const sexes = mainState.sexes
    let phone_number = null
    const user_name = (mainState.user_name) ? mainState.user_name.toString().trim() : ''


    if(user_name.length < 2 || user_name.length > 30){
      showMessage({
        message: "В имени должно быть больше 1 и меньше 30 символов",
        type: "warning",
      })    
      return 
    }

    if( !(/^[a-zA-Zа-яА-ЯёЁ0-9 -]{2,30}$/u.test(user_name)) ){
      showMessage({
        message: "Имя может состоять из букв русского или английского алфавитов, цифр, пробела, тире",
        type: "warning",
      })    
      return 
    }
    

    if(phoneNumberValue.length > 2 && isValidPhoneNumber){
      phone_number = phoneNumberValue
    }else if(phoneNumberValue.length > 2 && !isValidPhoneNumber){
      showMessage({
        message: "В номере телефона есть ошибка",
        type: "warning",
      })    
      return   
    }

    if ( !(sexes.includes(sex)) ) { 
      showMessage({
        message: "Вы не указали ваш пол",
        type: "warning",
      })
      return
    }else if ( !(sexes.includes(sex_need)) ) {
      showMessage({
        message: "Вы не указали пол людей для знакомств",
        type: "warning",
      })
      return
    }else if ( !birth_date ) {
      showMessage({
        message: "Вы не указали дату вашего рождения",
        type: "warning",
      })
      return
    }else{
      await mainState.saveSettings(sex, sex_need, birth_date, phone_number, user_name)
      mainState.set( {name: 'phone_number', value: phone_number} )
      return
    }
    
  }

  _signOutAsync = async () => {
    await this.props.mainState.signOut()
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'column',
    flexWrap: 'nowrap',
  },
  pickerContainer: {
      flex: 0,
      flexDirection: 'row',
      alignItems: 'center'
  },
  buttonContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop:20,
  },
  pickerTitle: {
      textAlign: 'center',
      fontSize: 16,
  },
  picker: {
      height: 50,
      width: 150,
  },
  picker2: {
      height: 50,
      width: 170,
  },
  picker3: {
      height: 50,
      width: 170,
      marginLeft: 30,
  },
  pickerItem: {
    fontSize: 16,
  },
});