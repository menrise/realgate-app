import React from 'react';
import { ScrollView, StyleSheet, Text, View, Button, Alert, Platform } from 'react-native'
import { List, ListItem, SectionList, Icon } from 'react-native-elements'
import { observer, inject } from 'mobx-react/native'
import { timeFormatUser, timeFormatDB } from './../../helpers/time'
import { makeId } from './../../helpers/misc'
import Loading from './../../components/Loading'
import SelectedMyMeet from './SelectedMyMeet'
import * as Animatable from 'react-native-animatable'


@inject('mainState')
@observer
export default class MyMeetsListScreen extends React.Component {

  static navigationOptions = {
    title: 'Мои встречи',
  }


  render() {

    const { mainState } = this.props

    if(mainState.selectedMyMeet) {
      return (
        <View >
          <Button title="К списку встреч" onPress={ () => mainState.setSelectedMyMeet(false) } />
          <SelectedMyMeet />
        </View>
      )
    }


    if(mainState.meetsLoading) return (<Loading />)

    const userMeetsInPorgress = mainState.userMeetsInPorgress
    const userMeetsFuture = mainState.userMeetsFuture
    const userMeetsPast = mainState.userMeetsPast


    const emptyItem = () =>
     (<ListItem 
        key={ makeId() }
        title="-"
        hideChevron
      />)

    const meetItem = (meet, inProgressMeet) => {

      const meetGroupTitle = mainState.getMeetGroupData(meet.user_group).title

      if(inProgressMeet) {

        return <Animatable.View 
            duration={1000}
            animation="pulse"
            easing="ease-out"
            iterationCount="infinite"
            useNativeDriver={true}
            key={ makeId() }
          >
            <ListItem
              containerStyle={ (inProgressMeet) ? {backgroundColor: '#c2f5c2'} : null }
              roundAvatar
              avatar={ (meet.place && meet.place.photos && meet.place.photos[0]) 
              ? {uri: mainState.google.getPhotoUrl(meet.place.photos[0].photo_reference, 250, 250)} 
              : require('./../../assets/images/mall.png') }
              key={ (meet.place) ? meet.composite_meet_id : makeId() }
              title={ (meet.place) ? meet.place.name + ' - (' + meetGroupTitle + ')' : meetGroupTitle }
              subtitle={timeFormatUser(meet.meet_time)}
              onPress={ () => mainState.navigation.navigate("MeetTime") }
            />
          </Animatable.View>

      }else{

        return <ListItem
          containerStyle={ (inProgressMeet) ? {backgroundColor: '#c2f5c2'} : null }
          roundAvatar
          avatar={ (meet.place && meet.place.photos && meet.place.photos[0]) 
          ? {uri: mainState.google.getPhotoUrl(meet.place.photos[0].photo_reference, 250, 250)} 
          : require('./../../assets/images/mall.png') }
          key={ (meet.place) ? meet.composite_meet_id : makeId() }
          title={ (meet.place) ? meet.place.name + ' - (' + meetGroupTitle + ')' : meetGroupTitle }
          subtitle={timeFormatUser(meet.meet_time)}
          onPress={ () => mainState.setSelectedMyMeet(meet) }
        />

      }
    }


    return (
      <ScrollView style={styles.container}>


        <View style={styles.sectionContainer}>
          <Text style={styles.sectionTitle}>Текущие: </Text>
        </View>
        <List
          containerStyle={{marginTop: 0}}>
          {
            (userMeetsInPorgress.length < 1) ? emptyItem() : userMeetsInPorgress.map( (meet) => meetItem(meet, true) )
          }
        </List>


        <View style={styles.sectionContainer}>
          <Text style={styles.sectionTitle}>Предстоящие: </Text>
        </View>
        <List
          containerStyle={{marginTop: 0}}>

          {
            (userMeetsFuture.length < 1) ? emptyItem(): userMeetsFuture.map( (meet) => meetItem(meet) )
          }

        </List>


        <View  style={{flexDirection:'row', flexWrap:'wrap', backgroundColor: '#efefef'}}>
          <Text style={styles.sectionTitle}>Прошедшие: </Text>
          <Icon
            name={Platform.OS === 'ios' ? `question-circle-o` : 'question-circle-o'}
            type='font-awesome'
            color='#bbb'
            size={18}
            containerStyle={styles.sectionHelpIcon}
            onPress={ () => Alert.alert('Прошедшие встречи',
              'В этом списке выводятся встречи за последний месяц, поэтому чтобы не потерять интересного человека сохраняйте к себе в телефон контакты при взаимных симпатиях.'
            )}
          />
        </View>
        <List
          containerStyle={{marginTop: 0}}>

          {
            (userMeetsPast.length < 1) ? emptyItem(): userMeetsPast.map( (meet) => meetItem(meet) )
          }

        </List>

      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  sectionContainer: {
    backgroundColor: '#efefef',
  },
  sectionTitle: {
    color: '#666',
    fontSize: 14,
    marginBottom: 8,
    marginLeft: 16,
    marginTop: 24,
    opacity: 0.8,
  },
  sectionHelpIcon:{
    marginBottom: 8,
    marginTop: 24,
    opacity: 0.8,
  }
});
