
import { View, Text, StyleSheet, Button, Image, ScrollView } from 'react-native'
import React from 'react'
import { observer, inject } from 'mobx-react/native'
import { Card } from 'react-native-elements'
import { timeFormatUser, timeFormatDB, isBeforeThanNow } from './../../helpers/time'
import { MapView } from 'expo'
import { ConfirmDialog } from 'react-native-simple-dialogs'
import UserBadge from './../../components/UserBadge'
import { makeId } from './../../helpers/misc'
import MatuallyLikedUsers from './../../components/MatuallyLikedUsers'
import { MeetInfo } from './../../constants/Texts'


@inject('mainState')
@observer
export default class SelectedMyMeet extends React.Component {

  constructor(props) {
    super(props)
    this.state = { confirmUnmemberVisible: false }
  }

  componentWillUnmount(){
    this.setState({confirmUnmemberVisible: false})
  }


  getMutuallyLikedUsers(likes, user_id){

    const mutuallyLikedUsers = likes.map( (user1) => {
      if(user1.pivot.liker_user_id === user_id){

        const isMutually = likes.some( (user2) => {
          if(user2.pivot.liker_user_id === user1.id && user2.pivot.liked_user_id === user_id) return true
        })

        if(isMutually) return user1
      }
    })

    const mutuallyLikedUsersCleared = mutuallyLikedUsers.filter(element => element !== undefined)

    return mutuallyLikedUsersCleared
  }

  render() {
    const { mainState } = this.props
    const meet = mainState.selectedMyMeet

    const timeFormatedForDB = timeFormatDB(meet.meet_time) 
    const timeFormatedForUser = timeFormatUser(meet.meet_time) 
    const memberOfMeet = true 

    const isMeetPast = isBeforeThanNow(meet.meet_time)

    let membersString = ''
    membersString += (meet.user_groups.max_mans) ? meet.man_count + '/' + meet.user_groups.max_mans + 'м' : ''
    membersString += (membersString && meet.user_groups.max_womans) ? ' - ' : ''
    membersString += (meet.user_groups.max_womans) ? meet.woman_count + '/' + meet.user_groups.max_womans + 'ж' : ''
    const userGroup = meet.user_group

    const coords = {
      latitude: meet.place.geometry.location.lat,
      longitude: meet.place.geometry.location.lng,
    }

    return (
      <View>
        <ScrollView contentContainerStyle={styles.scrollContainer}>
          <Card 
            image={ (meet.place && meet.place.photos && meet.place.photos[0]) 
                ? {uri: mainState.google.getPhotoUrl(meet.place.photos[0].photo_reference, 250, 250)} 
                : require('./../../assets/images/mall.png') }
            containerStyle={styles.meetCard}
            title={meet.place.name + ' (' + meet.user_groups.description + ')'}>
            <Text style={styles.placeDescription}>{meet.place.vicinity}</Text>
            <Text style={styles.meetInfo}>{MeetInfo}</Text>
            <Text style={styles.simpletext}></Text>


            <MapView
              style={{ alignSelf: 'stretch', height: 200 }}
              region={{ latitude: coords.latitude, longitude: coords.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 }}
              showsUserLocation={true}
              showsMyLocationButton={true}
            >

              <MapView.Marker
                key={meet.place.id}
                coordinate={coords}
                title={meet.place.name}
                description={meet.place.vicinity}
              >
              </MapView.Marker>

            </MapView>

            <Text style={styles.simpletext}>
              Участники: {membersString}
            </Text>

            <Text style={styles.simpletext}>
              Время встречи - {timeFormatedForUser}
            </Text>

            <Text style={styles.simpletext} >
              {
                (memberOfMeet) ? 
                  (isMeetPast) ? '- вы участвовали в этой встрече' : ' - вы участвуете в этой встрече'
                : null 
              }
            </Text>

            { (memberOfMeet && isMeetPast) ? <MatuallyLikedUsers likes={meet.likes} user_id={mainState.user_id} /> : null }

            { 
              (!isMeetPast) ? <Button title="отменить участие"
                disabled={mainState.unorderMeetInProcess}
                onPress={
                  () => {
                    this.setState({confirmUnmemberVisible: true})
                  }
                } >
              </Button> : null
            }
            
          </Card>

          <ConfirmDialog
            title="Отмена участия"
            message="Вы уверены что хотите отменить участие?"
            visible={this.state.confirmUnmemberVisible}
            onTouchOutside={ () => this.setState({confirmUnmemberVisible: false}) }
            positiveButton={{
                title: "Да",
                onPress: async () => {
                  await mainState.unorderMeet(timeFormatedForDB, userGroup, meet.place)
                  mainState.setSelectedMyMeet(false)
                }
            }}
            negativeButton={{
                title: "Нет",
                onPress: () => this.setState({confirmUnmemberVisible: false})
            }}
          />
        </ScrollView>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  scrollContainer: {
    paddingBottom: 80,
  },
  simpletext: {
    margin: 10,
  },
  placeDescription:{
    textAlign: 'center',
  },
  meetInfo:{
    textAlign: 'center',
    fontSize: 12,
    color: '#666'
  },
  meetCard: {
    borderRadius:3,
  }
})