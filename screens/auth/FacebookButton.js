import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { observer, inject } from "mobx-react/native"
import { Button } from 'react-native-elements'
import Colors from './../../constants/Colors'


@inject('mainState')
@observer
export default class App extends React.Component {

  render() {
    return (
      <View style={styles.container}>
        <Button
          title="Facebook"
          onPress={this._auth}
          icon={{name: 'facebook', type: 'font-awesome'}}
          raised
          borderRadius={2}
          containerViewStyle={{borderRadius: 3}}   
          buttonStyle={{backgroundColor: Colors.tintColor}}        
        />
      </View>
    )
  }

  _auth = async () => {
    await this.props.mainState.facebook.auth()
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});