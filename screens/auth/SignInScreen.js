import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
  Text,
} from 'react-native'
import AuthFacebookButton from './FacebookButton'
import AuthVkontakteButton from './VkontakteButton'
import AuthGoogleButton from './GoogleButton'
import AuthInstagramButton from './InstagramButton'
import { observer, inject } from 'mobx-react/native'
import { action } from 'mobx'
import Anchor from './../../components/Anchor'
import { backendUrl } from './../../constants/Apis'

@inject('mainState')
@observer
export default class SignInScreen extends React.Component {

  constructor(props) {
    super(props)
  }

  static navigationOptions = {
    title: 'Войти через',
  }

  componentWillMount(){
    const { mainState } = this.props

    if(mainState.jwtToken){
      this.props.navigation.navigate("App")
    }else{
      this.props.navigation.navigate("Auth")
    }    
  }

  render() {
    return (
      <View style={styles.container}>
        <AuthVkontakteButton />
        <AuthFacebookButton />
        <AuthInstagramButton />
        <AuthGoogleButton />
        
        <View style={styles.licenseBlock}>
            <Text style={styles.licenseText}>Используя приложение вы полностью принимаете </Text>
            <Anchor style={styles.licenseText} href={backendUrl + 'license'} >пользовательское соглашение</Anchor>
            <Text style={styles.licenseText}> и </Text>
            <Anchor style={styles.licenseText} href={backendUrl + 'privacy-policy'} >политику конфиденциальности</Anchor>
        </View>
        
      </View>
    )
  }
  
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: '25%',
    paddingBottom: '10%',
  },
  licenseBlock: {
    marginTop: 20,
    padding: 10,
  },
  licenseText:{
    textAlign: 'center',
    color: '#bbb',
    fontSize: 12,
  }
  
});