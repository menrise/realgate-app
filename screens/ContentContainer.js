import React from 'react'
import { Platform, StatusBar, StyleSheet, View} from 'react-native'
import { Notifications, Linking, KeepAwake } from 'expo'
import AppNavigator from './../navigation/AppNavigator'
import { observer, inject } from 'mobx-react/native'
import store from './../stores/mobx'
import FlashMessage from 'react-native-flash-message'
import { showMessage } from 'react-native-flash-message'
import { handleAuthRedirect } from '../helpers/requests'
import { cloneViaJson, makeId } from '../helpers/misc'
import { toJS } from 'mobx'


@inject('mainState')
@observer
export default class ContentContainer extends React.Component {

  google_place_id_for_query
  google_place_id
  mainState

  constructor(props) {
    super(props)

    this.notificationHandler = this.notificationHandler.bind(this)
  }

  componentDidMount() {
    const { mainState, meet } = this.props
    this.mainState = mainState

    Linking.addEventListener('url', handleAuthRedirect)

    if (Platform.OS === 'android') {
      Notifications.createChannelAndroidAsync('reminders', {
        name: 'Напоминания о встречах',
        sound: true,
        priority: 'max',
        vibrate: [0, 250, 250, 250],
      })
    }
    
    // Handle notifications that are received or selected while the app
    // is open. If the app was closed and then opened by tapping the
    // notification (rather than just tapping the app icon to open it),
    // this function will fire on the next tick after the app starts
    // with the notification data.
    this._notificationSubscription = Notifications.addListener(this.notificationHandler)

  }

  async notificationHandler (notification) {

      this.mainState.set({name: 'lastPushNotification', value: notification})
      showMessage({
        message: notification.data.name,
        description: notification.data.description,
        type: "success",
      })

      if(notification.data.google_place_id_for_query){
        this.mainState.set({ name: 'push_initiated_popup', value: {} })
        this.mainState.set( {name: 'push_initiated_popup',
          value: {
            google_place_id: notification.data.google_place_id,
            google_place_id_for_query: notification.data.google_place_id_for_query,
          }
        })      
        
        this.mainState.navigate("OrderMeet")
      }
  }


  render() {
    const { mainState } = this.props
    mainState.set({name: 'navigation', value: store.navigationStore})

		return (
			<View style={styles.container}>
				{Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <KeepAwake />
				<AppNavigator ref={store.navigationStore.createRef}/>
				<FlashMessage position="top" />
			</View>
		)
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
})
