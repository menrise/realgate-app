import React, { Component } from 'react'
import { Text, View, StyleSheet, Button, Alert, TouchableHighlight, Image, Platform, ScrollView } from 'react-native'
import { Constants, MapView, Permissions } from 'expo'
import { observer, inject } from 'mobx-react/native'
import { timeFormatDB } from './../../helpers/time'
import { man_count, woman_count, memberOfMeet } from './../../helpers/misc'
import { Card, Icon } from 'react-native-elements'
import Modal from 'react-native-modal'
import MeetCards from './MeetCards'
import { MeetInfo } from './../../constants/Texts'


@inject('mainState')
@observer
export default class CurrentMeetPlaceModal extends Component {

  static navigationOptions = {
    title: 'Назначить встречу',
  }

  state = {
    scrollOffset: 0
  }

  setModalVisible(visible) {
    this.props.mainState.setCurrentMeetPlaceModalVisible(visible)
  }

  handleOnScroll = event => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y
    })
  }

  handleScrollTo = p => {
    if (this.scrollViewRef) {
      this.scrollViewRef.scrollTo(p);
    }
  }

  render() {
    const { mainState } = this.props

    return (
      <View style={styles.container}>

        <Modal 
          backdropOpacity={0.5}
          isVisible={mainState.currentMeetPlaceModalVisible}
          onBackdropPress={() => mainState.setCurrentMeetPlaceModalVisible(false) }
          onSwipe={() => mainState.setCurrentMeetPlaceModalVisible(false) }
          //swipeDirection="right"
          scrollTo={this.handleScrollTo}
          scrollOffset={this.state.scrollOffset}
          >
            <View style={{marginTop: 52}}>
              <Card 
                image={ (mainState.current_meet_place.photos && mainState.current_meet_place.photos[0]) 
                  ? {uri: mainState.google.getPhotoUrl(mainState.current_meet_place.photos[0].photo_reference, 250, 250)} 
                  : require('./../../assets/images/mall.png') }
                containerStyle={styles.modalCard} 
                title={mainState.current_meet_place.name}
                >

                <View style={{height: 250}}>
                  <ScrollView
                    ref={ref => (this.scrollViewRef = ref)}
                    onScroll={this.handleOnScroll}
                    scrollEventThrottle={16}
                  >

                    <View >
                      <View>
                        <TouchableHighlight
                          onPress={() => {
                            this.setModalVisible(!mainState.currentMeetPlaceModalVisible)
                          }}>
                          <Icon
                            style={styles.close}
                            name={Platform.OS === 'ios' ? `ios-close` : 'md-close'}
                            type='ionicons'
                            size={24} 
                            color='#black'
                            />       
                        </TouchableHighlight>
                      </View>

                      <View>
                        <Text style={styles.placeDescription}>{mainState.current_meet_place.vicinity}</Text>
                        <Text style={styles.meetInfo}>{MeetInfo}</Text>
                        <Text style={styles.simpletext}></Text>
                        <Text style={styles.simpletext}>Встречи подходящие вам:</Text>

                        <MeetCards />
                      </View>
                    </View>
                  
                  </ScrollView>
                </View>

              </Card>
            </View>

          
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  callout: {
    margin: 10,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  simpletext: {
    margin: 10,
  },
  close: {
    alignSelf: 'flex-end',
  },
  placeDescription:{
    textAlign: 'center',
  },
  meetInfo:{
    textAlign: 'center',
    fontSize: 12,
    color: '#666',
  },
  meetCard: {
    borderRadius:3,
  },
  modalCard: {
    borderRadius:3,
  }

});