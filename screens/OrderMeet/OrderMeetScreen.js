import React, { Component } from 'react'
import { Text, View, StyleSheet, Button, Alert, Modal, TouchableHighlight, NetInfo } from 'react-native'
import { Constants, MapView, Permissions, Location } from 'expo'
import { observer, inject } from 'mobx-react/native'
import { timeFormatUser } from './../../helpers/time'
import CurrentMeetPlaceModal from './CurrentMeetPlaceModal'
import Loading from './../../components/Loading'
import { Badge } from 'react-native-elements'
import StaticNotice from './StaticNotice'
import { getLocationDataAsync } from './../../helpers/requests'
import { showMessage } from 'react-native-flash-message'
import { stepForMeetGroup1vs1 } from './../../constants/System'
import { getRandomInt } from './../../helpers/misc'


@inject('mainState')
@observer
export default class OrderMeetScreen extends Component {

  static navigationOptions = {
    title: 'Участвовать во встрече',
  }

  checkLocationServicesEnabledInterval = null
  isInitedData = false

  state = {
    permissionError: null, //'Вы не дали разрешение на геолокацию. Мы не можем найти вашу позицию на карте.',
    locationServicesEnabled: true,
  }

  constructor(props) {
    super(props)

    this.askPermission = this.askPermission.bind(this)
    this.initData = this.initData.bind(this)
    this.locationServicesEnabledWatcher = this.locationServicesEnabledWatcher.bind(this)
    this.checkLocationServicesEnabled = this.checkLocationServicesEnabled.bind(this)
    this.netConnectionWatcher = this.netConnectionWatcher.bind(this)
    this.handleConnectivityChange = this.handleConnectivityChange.bind(this)
  }

  componentWillMount() {
    const { mainState } = this.props
    if ( !(mainState.sexes.includes(mainState.sex)) ) { 
      Alert.alert('Вы не указали ваш пол', 'Это можно сделать в настройках')
      return
    }else if ( !(mainState.sexes.includes(mainState.sex_need)) ) {
      Alert.alert('Вы не указали пол людей для знакомств', 'Это можно сделать в настройках')
      return
    }
  }

  componentDidMount() {
    setTimeout(this.netConnectionWatcher, 1)
  }

  async componentWillUpdate(){
    const { mainState } = this.props

    //handle push initiated popup
    if(mainState.push_initiated_popup.google_place_id_for_query){
      let place
      if(mainState.push_initiated_popup.google_place_id){
        place = mainState.getMeetPlace(mainState.push_initiated_popup.google_place_id)
      }
      
      if(!place){
        place = await mainState.google.getPlace(mainState.push_initiated_popup.google_place_id_for_query)

        mainState.addMeetPlaceAddition(place)
        await this.initData()        
      }

      mainState.set({ 
        name: 'push_initiated_popup_google_place_id_for_query', 
        value:  mainState.push_initiated_popup.google_place_id_for_query
      })
      mainState.set({ name: 'push_initiated_popup', value: {} })
      if(place){
        mainState.setCurrentPlace(place)
        mainState.setCurrentMeetPlaceModalVisible(true) 
      }  
      
    }

  }

  async initData(){
    const { mainState } = this.props

    mainState.set({name: 'isMapDataLoading', value: true})

    await this.checkPermission()
    await this.locationServicesEnabledWatcher()
    await mainState.getMeets(true)
    await this.askPermission()  

    mainState.set({name: 'isMapDataLoading', value: false})
  }

  async netConnectionWatcher(){
    await this.initData()

    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.handleConnectivityChange
    )
  }

  async handleConnectivityChange(isConnected) {
    console.log('Internet connection change to ' + (isConnected ? 'online' : 'offline'));
    
    if(isConnected){
      showMessage({
        message: "Интернет соединение восстановлено",
        type: "success",
        duration: 3000
      })
      //first net ok - initData
      if(!this.isInitedData){
        this.isInitedData = true
        await this.initData()
      }
    }else{
      showMessage({
        message: "Интернет соединение потеряно",
        type: "warning",
        duration: 5000
      })
    }
  }

  async locationServicesEnabledWatcher(){
    const locationServicesEnabled = await this.checkLocationServicesEnabled()

    if(!locationServicesEnabled && !this.checkLocationServicesEnabledInterval){
      this.checkLocationServicesEnabledInterval = setInterval(this.checkLocationServicesEnabled, 1000)
    }
  }

  async checkLocationServicesEnabled(){
    let locationServicesEnabled = await Location.hasServicesEnabledAsync()
    
    if(locationServicesEnabled){
      if(this.state.locationServicesEnabled !== true){
        this.setState({
          locationServicesEnabled: true,
        })    

        if(this.checkLocationServicesEnabledInterval){
          clearTimeout(this.checkLocationServicesEnabledInterval)
          await this.askPermission()
        }
      }
    }else{
      if(this.state.locationServicesEnabled !== false){
        this.setState({
          locationServicesEnabled: false,
        })      
      }
    } 

    return locationServicesEnabled
  }

  async checkPermission(){
    let locationPermission = await Permissions.getAsync(Permissions.LOCATION)
    let status = locationPermission.status
    let permissionError

    if(status !== 'granted'){
      permissionError = 'Разрешите геолокацию чтобы увидеть ближайшие места встреч.'
      if(this.state.permissionError !== permissionError){
        this.setState({
          permissionError: permissionError,
        })    
      }    
    }else{
      permissionError = null
      if(this.state.permissionError !== permissionError){
        this.setState({
          permissionError: permissionError,
        })   
      }     
    }   

    return locationPermission
  }

  async askPermission(){
    let { status } = await Permissions.askAsync(Permissions.LOCATION)
    if (status !== 'granted') {
      return false
    }

    await this.checkPermission()
    await getLocationDataAsync()
  }


  activateModal(marker){
    const { mainState } = this.props
    mainState.setCurrentMeetPlaceModalVisible(!mainState.currentMeetPlaceModalVisible)
  }

  searchNeedMeMeetPlaces(){
    const { mainState } = this.props
    
    let group = {}
    const meet_places_where_need_me = mainState.meet_places.filter((meet_place) => {

      //1vs1 group
      const numberGroup1vs1 = mainState.user_group + stepForMeetGroup1vs1
      group = meet_place.groups['meetGroup'+numberGroup1vs1]

      if(
        mainState.sex === 1 && group.sex_composition_id === 1
        && group.manСount < group.maxMan
        && group.womanСount > group.manСount
      ){
        return true
      }else if(
        mainState.sex === 2 && group.sex_composition_id === 1
        && group.womanСount < group.maxWoman
        && group.manСount > group.womanСount
      ){
        return true
      }else if(
        mainState.sex === 1 && group.sex_composition_id === 2
        && group.manСount < group.maxMan
        && group.manСount > 1
      ){
        return true
      }else if(
        mainState.sex === 2 && group.sex_composition_id === 3
        && group.womanСount < group.maxWoman
        && group.womanСount > 1
      ){
        return true
      }


      //multi pairs group
      group = meet_place.groups['meetGroup'+mainState.user_group]

      if(
        mainState.sex === 1 && group.sex_composition_id === 1
        && group.manСount < group.maxMan
        && group.womanСount > group.manСount
      ){
        return true
      }else if(
        mainState.sex === 2 && group.sex_composition_id === 1
        && group.womanСount < group.maxWoman
        && group.manСount > group.womanСount
      ){
        return true
      }else if(
        mainState.sex === 1 && group.sex_composition_id === 2
        && group.manСount < group.maxMan
        && group.manСount > 1
      ){
        return true
      }else if(
        mainState.sex === 2 && group.sex_composition_id === 3
        && group.womanСount < group.maxWoman
        && group.womanСount > 1
      ){
        return true
      }

      return false

    })

    
    if(meet_places_where_need_me.length){
      const meet_place_randomed = meet_places_where_need_me[getRandomInt(0, meet_places_where_need_me.length - 1)]

      mainState.set({ 
        name: 'push_initiated_popup_google_place_id_for_query', 
        value:  meet_place_randomed.place_id
      })

      const place = mainState.getMeetPlace(meet_place_randomed.id)

      if(place){
        mainState.setCurrentPlace(place)
        mainState.setCurrentMeetPlaceModalVisible(true) 
      }  
    
    }else{
      Alert.alert('Выберите любое место встречи', `Встреч на которых не хватает людей, соответствующих вашим настройкам, в данный момент поблизости нет.

Выбирайте любое удобное вам место встречи (люди рядом будут оповещены о встрече).

Места встреч обозначены красными маркерами на карте.`)
    }
    
  }


  render() {
    const { mainState } = this.props

    //const user_meets = this.user_meets //for reaction of state

    const google_place_id = mainState.current_meet_place.id
    let current_place = mainState.getMeetPlace(google_place_id)

    const mapCenterCoords = {
      latitude: (current_place) ? current_place.geometry.location.lat : mainState.user_location.coords.latitude,
      longitude: (current_place) ? current_place.geometry.location.lng : mainState.user_location.coords.longitude,
    }

    //for reaction of push popup by async 
    const google_place_id_for_query = mainState.push_initiated_popup.google_place_id_for_query
    

    return (
      <View style={styles.outerContainer}>
        <View style={styles.container}>

          <MapView
            style={{ alignSelf: 'stretch', height: (this.state.permissionError != null || !this.state.locationServicesEnabled) ? '60%' : '100%' }}
            region={{ latitude: mapCenterCoords.latitude, longitude: mapCenterCoords.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 }}
            showsUserLocation={true}
            showsMyLocationButton={true}
          >

            {mainState.meet_places.map((marker, index) => {

              if(!marker) return null

              const coords = {
                latitude: marker.geometry.location.lat,
                longitude: marker.geometry.location.lng,
              }

              const google_place_id = mainState.current_meet_place.id
              let current_place = mainState.getMeetPlace(google_place_id)
              current_place = (current_place) ? current_place : { groups: mainState.meetGroupsTemplate }
              current_place_groups = current_place.groups

              return (
                <MapView.Marker
                  key={index}
                  coordinate={coords}
                  title={marker.name}
                  description={marker.vicinity}
                  onPress={() => mainState.setCurrentPlace(marker) }
                >
                  <MapView.Callout onPress={() => this.activateModal(marker) }>
                    <View style={styles.callout}>
                      <Text style={styles.paragraph}>{marker.name}</Text>
                      <Text style={styles.simpletext}>{marker.vicinity}</Text>
                      <Text style={styles.simpletext}>Встречи:</Text>

                      {
                        Object.keys(current_place_groups).map(function(key) {
                          if(current_place_groups[key].sex_composition_id === 1){
                            return <Text key={key}>
                              {current_place_groups[key].title}
                              {/* &nbsp;- { (current_place_groups[key].maxMan) ? current_place_groups[key].maxMan + 'м' : null} */}
                              {/* { (current_place_groups[key].maxMan && current_place_groups[key].maxWoman) ? '/' : null} */}
                              {/* { (current_place_groups[key].maxWoman) ? current_place_groups[key].maxWoman + 'ж' : null} */}
                              &nbsp;- {current_place_groups[key].timeFormatUser}
                            </Text>
                          }
                        })
                      }
                      <Text style={styles.simpletext}>и др.</Text>
                      <Text style={styles.simpletext}></Text>
                      <Button title='Встретиться здесь' onPress={() => true} />
                    </View>
                  </MapView.Callout>
                </MapView.Marker>
              )

            })} 



          </MapView>
          {
            (this.state.permissionError != null && this.state.locationServicesEnabled)
            ? 
            <View style={styles.geoAccessButtonContainer}>
              <Text style={styles.paragraph}>{this.state.permissionError}</Text>
              <Button title="Разрешить геолокацию" onPress={this.askPermission} />
            </View>
            :
            <View></View>
          }

          {
            (!this.state.locationServicesEnabled)
            ? 
            <View style={styles.geoAccessButtonContainer}>
              <Text style={styles.paragraph}>У вас отключена геолокация. Приложение не может нормально работать.

              Включите "Данные о местоположении" в настройках смартфона.</Text>
            </View>
            :
            <View></View>
          }

          <CurrentMeetPlaceModal />

        </View>

        {
          ( mainState.userMeetsInPorgress.length )
            ? <StaticNotice text="Время встречи"
              onPressFunction={() => mainState.navigation.navigate("MeetTime")} />
            : null
        }

        {
          ( !mainState.isMapDataLoading && !mainState.userMeetsInPorgress.length && !mainState.userMeetsFuture.length && mainState.sex > 0 )
            ? <StaticNotice text="Где не хватает участников?"
              onPressFunction={() => this.searchNeedMeMeetPlaces()}/>
            : null
        }

        {
          ( !mainState.userMeetsInPorgress.length && !mainState.userMeetsFuture.length && mainState.sex < 1 )
            ? <StaticNotice text="Сначала задайте настройки"
              onPressFunction={() => mainState.navigation.navigate("Settings")} />
            : null
        }

        {
          ( mainState.isMapDataLoading )
          ? 
            <View style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 0,
              bottom: '90%',
              alignItems: 'center',
              justifyContent: 'center',
              zIndex: 2,
            }}>
              <Loading/>
            </View>
          : null
        }

      </View>
    );
  }
}

const styles = StyleSheet.create({
  outerContainer:{
    height: '100%',
    width: '100%',  
  },
  container: {
    display: 'flex',
    flex: 4,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#ecf0f1',
    height: '100%',
    width: '100%',
    zIndex: 1,
    position: 'absolute',
    top: 0, 
    left: 0,   
  },
  geoAccessButtonContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
    height: '40%',
    width: '100%',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  callout: {
    margin: 10,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  simpletext: {
    margin: 10,
  },
  close: {
    margin: 10,
    alignSelf: 'flex-end',
  },
  placeDescription:{
    textAlign: 'center',
  },
});