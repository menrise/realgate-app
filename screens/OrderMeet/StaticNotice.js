import React, { Component } from 'react'
import { Text, View, StyleSheet} from 'react-native'
import { observer, inject } from 'mobx-react/native'
import { Badge } from 'react-native-elements'
import * as Animatable from 'react-native-animatable'


@inject('mainState')
@observer
export default class StaticNotice extends Component {

  render() {
    
    const { mainState, text, onPressFunction } = this.props

    return (
      <View style={styles.outerContainer}>

        <Badge
          containerStyle={{
            backgroundColor: '#c2f5c2',
            elevation:4,
            shadowOffset: { width: 5, height: 5 },
            shadowColor: "grey",
            shadowOpacity: 0.5,
            shadowRadius: 10, 
            marginTop: 15,
            marginBottom: 5,
            marginLeft: 25,
            marginRight: 25,
            borderWidth: 1,
            borderColor: '#bbb',
          }}
          wrapperStyle={{
            zIndex: 1,
          }}
          onPress={ onPressFunction }
        >

          <Animatable.View 
            duration={1000}
            animation="pulse"
            easing="ease-out"
            iterationCount="infinite"
            useNativeDriver={true}
          >
            <Text           
              style={{
                fontSize: 18,
                padding: 5,
              }}
            >
              {text}
            </Text>
          </Animatable.View>

        </Badge>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  outerContainer:{

  },
});