import React, { Component } from 'react'
import { Text, View, StyleSheet, Button } from 'react-native'
import { observer, inject } from 'mobx-react/native'
import { Card } from 'react-native-elements'
import { cloneDeep } from 'lodash'
import Loading from './../../components/Loading'
import MeetCard from './MeetCard'
import { stepForMeetGroup1vs1 } from './../../constants/System'


@inject('mainState')
@observer
export default class MeetCards extends Component {

  render() {
    const { mainState } = this.props

    const google_place_id = mainState.current_meet_place.id
    const current_place = mainState.getMeetPlace(google_place_id)

    if(current_place === undefined){
      return (<Loading/>)
    }

    if(!mainState.user_group){
      return (
        <View>
          <Text style={styles.notice}>
            Задайте настройки пола и возраста чтобы увидеть подходящие встречи.
          </Text>
        </View>
      )
    }

    //many pairs
    const meetGroup = current_place.groups['meetGroup' + mainState.user_group]

    //1vs1
    const user_group1vs1 = mainState.user_group + stepForMeetGroup1vs1
    const meetGroup1vs1 = current_place.groups['meetGroup' + user_group1vs1]
    
    return (
      <View>
        <MeetCard 
          meetGroup={meetGroup}
          userGroup={mainState.user_group}
        />

        <MeetCard 
          meetGroup={meetGroup1vs1}
          userGroup={user_group1vs1}
        />        
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  simpletext: {
    margin: 10,
  },
  notice: {
    margin: 10,
    fontWeight: "700",
  },
  meetCard: {
    borderRadius:3,
  }
})