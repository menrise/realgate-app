
import { View, Text, StyleSheet, Button, Alert } from 'react-native'
import React from 'react'
import { observer, inject } from 'mobx-react/native'
import { Card } from 'react-native-elements'
import { ConfirmDialog } from 'react-native-simple-dialogs'
import { isAfterThanNow } from './../../helpers/time'
import { minutesPeriodBeforeMeetUserCanSeeMembersCount } from './../../constants/System'
import { Icon } from 'react-native-elements'


@inject('mainState')
@observer
export default class MeetCard extends React.Component {

  constructor(props) {
    super(props)
    this.state = { confirmUnmemberVisible: false };
  }

  componentWillUnmount(){
    this.setState({confirmUnmemberVisible: false})
  }

  render() {

    const { mainState, meetGroup, userGroup } = this.props
    const { memberOfMeet, timeObject, timeFormatDB, timeFormatUser, title } = meetGroup    

    const displayPeopleCurrentCount = !isAfterThanNow(timeObject, -(minutesPeriodBeforeMeetUserCanSeeMembersCount))

    let membersJSX = []
    if(meetGroup.maxMan > 0 && meetGroup.maxWoman > 0){

      if(
        mainState.current_meet_place.place_id === mainState.push_initiated_popup_google_place_id_for_query
        || displayPeopleCurrentCount
      ){
        membersJSX.push(<Text>{meetGroup.manСount}</Text>)
      }else{
        membersJSX.push(
          <Icon iconStyle={styles.badge}
            name={Platform.OS === 'ios' ? `question-circle-o` : 'question-circle-o'}
            type='font-awesome'
            color='#bbb'
            onPress={ () => Alert.alert(
              'Текущее количество мужчин',
              'Данные станут видны в период ' + minutesPeriodBeforeMeetUserCanSeeMembersCount / 60 + ' часов перед встречей'
            ) }
          />
        )
      }

      membersJSX.push(<Text>/</Text>)
      membersJSX.push(<Text>{meetGroup.maxMan}</Text>) 
      membersJSX.push(<Text>м - </Text>)

      if(
        mainState.current_meet_place.place_id === mainState.push_initiated_popup_google_place_id_for_query
        || displayPeopleCurrentCount
      ){
        membersJSX.push(<Text>{meetGroup.womanСount}</Text>)
      }else{
        membersJSX.push(
          <Icon iconStyle={styles.badge}
            name={Platform.OS === 'ios' ? `question-circle-o` : 'question-circle-o'}
            type='font-awesome'
            color='#bbb'
            onPress={ () => Alert.alert(
              'Текущее количество женщин',
              'Данные станут видны в период ' + minutesPeriodBeforeMeetUserCanSeeMembersCount / 60 + ' часов перед встречей'
            ) }
          />
        )
      }

      membersJSX.push(<Text>/</Text>)
      membersJSX.push(<Text>{meetGroup.maxWoman}</Text>)
      membersJSX.push(<Text>ж</Text>)
    }else if(meetGroup.maxMan > 0 && meetGroup.maxWoman === 0){

      if(
        mainState.current_meet_place.place_id === mainState.push_initiated_popup_google_place_id_for_query
        || displayPeopleCurrentCount
      ){
        membersJSX.push(<Text>{meetGroup.manСount}</Text>)
      }else{
        membersJSX.push(
          <Icon iconStyle={styles.badge}
            name={Platform.OS === 'ios' ? `question-circle-o` : 'question-circle-o'}
            type='font-awesome'
            color='#bbb'
            onPress={ () => Alert.alert(
              'Текущее количество мужчин',
              'Данные станут видны в период ' + minutesPeriodBeforeMeetUserCanSeeMembersCount / 60 + ' часов перед встречей'
            ) }
          />
        )
      }

      membersJSX.push(<Text>/</Text>)
      membersJSX.push(<Text>{meetGroup.maxMan}</Text>)
      membersJSX.push(<Text>м</Text>)
    }else if(meetGroup.maxMan === 0 && meetGroup.maxWoman > 0){

      if(
        mainState.current_meet_place.place_id === mainState.push_initiated_popup_google_place_id_for_query
        || displayPeopleCurrentCount
      ){
        membersJSX.push(<Text>{meetGroup.womanСount}</Text>)
      }else{
        membersJSX.push(
          <Icon iconStyle={styles.badge}
            name={Platform.OS === 'ios' ? `question-circle-o` : 'question-circle-o'}
            type='font-awesome'
            color='#bbb'
            onPress={ () => Alert.alert(
              'Текущее количество женщин',
              'Данные станут видны в период ' + minutesPeriodBeforeMeetUserCanSeeMembersCount / 60 + ' часов перед встречей'
            ) }
          />
        )
      }

      membersJSX.push(<Text>/</Text>)
      membersJSX.push(<Text>{meetGroup.maxWoman}</Text>)
      membersJSX.push(<Text>м</Text>)
    }

    //TODO add meet balance control
    // test 1vs1 meet

    return (
      <View>
        <Card containerStyle={styles.meetCard} title={title}>

          <View style={styles.membersCountContainer}>
            <Text style={styles.simpletext}>Участники -</Text>
            {membersJSX}
          </View>

          <Text style={styles.simpletext}>
            Состоится - {timeFormatUser}
          </Text>

          <Text style={styles.simpletext} >{(memberOfMeet) ? ' - вы участвуете в этой встрече' : null }</Text>

          {(memberOfMeet) ? 

            <Button title="отменить участие"
              disabled={mainState.unorderMeetInProcess}
              onPress={
                () => this.setState({confirmUnmemberVisible: true})
              } >
            </Button>

            : 

            ( mainState.userMeetsFuture.length > 0 ) ?

              <Button
                title="Может быть только одна предстоящая встреча, после неё кнопка станет активна"
                disabled={true} 
                onPress={
                  () => false
                } >
              </Button>

              :

              ( 
                   (mainState.sex === 1 && meetGroup.manСount === meetGroup.maxMan)
                || (mainState.sex === 2 && meetGroup.womanСount === meetGroup.maxWoman)
              ) ?

                <Button
                  title="Больше нет мест"
                  disabled={true} 
                  onPress={
                    () => false
                  } >
                </Button>

                :

                <Button title="участвовать"
                  disabled={mainState.orderMeetInProcess}
                  onPress={
                    () => mainState.orderMeet(timeFormatDB, userGroup, mainState.current_meet_place)
                  } >
                </Button> 
          }
        </Card>

        <ConfirmDialog
          title="Отмена участия"
          message="Вы уверены что хотите отменить участие?"
          visible={this.state.confirmUnmemberVisible}
          onTouchOutside={ () => this.setState({confirmUnmemberVisible: false}) }
          positiveButton={{
              title: "Да",
              onPress: async () => {
                this.setState({confirmUnmemberVisible: false})
                mainState.unorderMeet(timeFormatDB, userGroup, mainState.current_meet_place)
              }
          }}
          negativeButton={{
              title: "Нет",
              onPress: () => this.setState({confirmUnmemberVisible: false})
          }}
        />
      </View>
    )
  }
}


const styles = StyleSheet.create({
  simpletext: {
    margin: 10,
  },
  meetCard: {
    borderRadius:3,
  },
  membersCountContainer: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center'
  },
  badge:{
    fontSize: 14,
  },
})